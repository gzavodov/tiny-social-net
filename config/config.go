package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

//Configuration Web server configuration
type Configuration struct {
	HTTPAddress       string   `json:"http_listen"`
	SessionSecret     string   `json:"session_secret"`
	DSN               string   `json:"dsn"`
	ReplicaDSN        []string `json:"replica_dsn,omitempty"`
	DbConnMaxLifetime uint16   `json:"db_conn_max_life_time"`
	MaxOpenDbConns    uint16   `json:"max_open_db_conns"`
	MaxIdleDbConns    uint16   `json:"max_idle_db_conns"`

	CacheInvalidationBatchSize uint16        `json:"cache_invalidation_batch_size"`
	CacheInvalidationDelay     time.Duration `json:"cache_invalidation_delay"`
}

//Load read configuration from file anf from os environment variables
func (c *Configuration) Load(filePath string, defaultVal *Configuration) error {
	if len(filePath) > 0 {
		if err := c.LoadFromFile(filePath); err != nil {
			return err
		}
	}

	if err := c.LoadFromEvironment(); err != nil {
		return err
	}

	c.setIfEmpty(defaultVal)

	return nil
}

func (c *Configuration) setIfEmpty(source *Configuration) {
	if source == nil {
		return
	}

	if c.HTTPAddress == "" {
		c.HTTPAddress = source.HTTPAddress
	}

	if c.DSN == "" {
		c.DSN = source.DSN
	}
}

//LoadFromFile read configuration from file
func (c *Configuration) LoadFromFile(filePath string) error {
	configFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("could not read configuration file: %w", err)
	}

	if json.Unmarshal(configFile, c) != nil {
		return fmt.Errorf("could not internalize configuration file data: %w", err)
	}

	return nil
}

//LoadFromEvironment read configuration from environment variables
func (c *Configuration) LoadFromEvironment() error {
	//End Point IP-Address
	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_HTTP_ADDRESS"); ok {
		c.HTTPAddress = s
	}

	//Session Secret
	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_SESSION_SECRET"); ok {
		c.SessionSecret = s
	}

	//DSN
	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_DSN"); ok {
		c.DSN = s
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_REPLICA_DSN"); ok {
		c.ReplicaDSN = strings.Split(s, "|")
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_DB_CONN_MAX_LIFE_TIME"); ok {
		v, err := strconv.ParseUint(s, 10, 16)
		if err != nil {
			return err
		}

		c.DbConnMaxLifetime = uint16(v)
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_MAX_OPEN_DB_CONNS"); ok {
		v, err := strconv.ParseUint(s, 10, 16)
		if err != nil {
			return err
		}

		c.MaxOpenDbConns = uint16(v)
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_MAX_IDLE_DB_CONNS"); ok {
		v, err := strconv.ParseUint(s, 10, 16)
		if err != nil {
			return err
		}

		c.MaxIdleDbConns = uint16(v)
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_CACHE_INVALIDATION_BATCH_SIZE"); ok {
		v, err := strconv.ParseUint(s, 10, 15)
		if err != nil {
			return err
		}

		c.CacheInvalidationBatchSize = uint16(v)
	}

	if s, ok := os.LookupEnv("TINY_SOCIAL_NET_CACHE_INVALIDATION_DELAY"); ok {
		d, err := time.ParseDuration(s)
		if err != nil {
			return err
		}

		c.CacheInvalidationDelay = d
	}

	return nil
}
