# Реализация кеширования

По условию задачи требуется:
- добавить функционал создание поста;
- добавить функционал подписки на посты пользователей;
- добавить функционал ленты новостей;
- лента новостей должна кешироваться;
- лента новостей формируется на уровне кешей;
- формирование ленты производить через постановку задачи в очередь на часть подписчиков, чтобы избежать эффекта леди Гаги;
- в ленте держать последние 1000 обновлений друзей;

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/85eecbd3eb2ebf4f64d2#?env%5Btiny-social-net%5D=W3sia2V5IjoiaG9zdCIsInZhbHVlIjoibG9jYWxob3N0OjkwOTAiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InBvc3RfaWQiLCJ2YWx1ZSI6IiIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoicHJldmlvdXNfdXNlcl9pZCIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJ1c2VyX2lkIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6Imp3dCIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZX1d)

Решение отличается от предложенного в задаче.
Схема работы с кешем ленты новостей следующая:
- кеш стоится в обработчике при первом запросе пользователя;
- изменения в сообщениях пользователя приводит только к сбросу (инвалидации) кеша фолловеров, перестроение откладывается до явного запроса;
- инвалидация кеша производится ступенчато - фолловеры разбиваются на группы, которые обрабатываются через определённый интервал времени.

Таким образом мы не тратим ресурсы на перестроение кеша ленты новостей для пользователей, которые могут за ней больше и не прийти. 
Напротив, мы строим кеш ленты при первом обращении.
Но с другой стороны мы защищаемся от "лавинообразного" перестроения кешей с помощью его ступенчатого сброса.

## Подписки

Подписка на обновления пользователей реализована через механизм фолловеров.
Обработчик API находится в [файле api/followers/handler.go](../api/followers/handler.go).
Поддерживаемые операции:
- `POST {{host}}/api/v2/follower/follow` включить подписку на сообщения пользователя, переданного в параметре `follow_to` в теле сообщения;
- `POST {{host}}/api/v2/follower/unfollow` выключить подписку на сообщения пользователя, переданного в параметре `follow_to` в теле сообщения;
- `GET {{host}}/api/v2/follower/follow-by/list/{{user_id}}` получить список пользователей, подписанных на переданного в параметре `user_id`
- `GET {{host}}/api/v2/follower/follow-to/list/{{user_id}}` получить список пользователей, на который подписан пользователь переданный в параметре `user_id`
 
## Посты

Обработчик API находится в [файле api/posts/handler.go](../api/posts/handler.go).
Поддерживаемые операции: 
- `POST {{host}}/api/v2/post/create` создание сообщения;
- `GET {{host}}/api/v2/post/get/{{post_id}}` чтение сообщения;
- `POST {{host}}/api/v2/post/update` обновление сообщения;
- `POST {{host}}/api/v2/post/delete` удаление сообщения. 

## Лента новостей

Обработчик API находится в [файле api/feeds/handler.go](../api/feeds/handler.go).
Поддерживаемые операции:
- `GET {{host}}/api/v2/feed/news` получить список сообщений пользователей, на обновления которых подписан текущий. Сообщения сортируются по дате создания в обратном порядке, т.е. более поздние сообщения будут показаны в начале ленты.  

## Кеширование

Кеширование ленты находиться в [файле api/caches/feed.go](../api/caches/feed.go).
Выбрано хранение кеша в памяти приложения, т.е. используется "бортовой кеш".
Причины почему не используется Redis или Memcached:
- в настоящий момент решение является монолитом, значит смысла в разделении кеша нет.
- лента новостей может иметь большой размер (кешируем до 1 000 сообщений), поэтому в варианте отдельного сервиса нам придётся передавать между процессами большие объёмы данных. Если сервис кеширования будет развёрнут на другом узле, то межпроцессный трафик становится ещё и сетевым, увеличивая время обработки запроса.  

Из недостатков данного решения можно указать холодный старт - т.е. приложение всегда стартует с пустым кешем. 

Пример работы с чтением/записью кеша можно посмотреть в [файле api/feeds/handler.go](../api/feeds/handler.go)

```golang
...

var (
    response = responses.New()
    userID   = h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
    content  []byte
    err      error
)

// Запрос закешированных данных
content, err = h.userFeedCacheProvider.GetEntry(userID)
if err != nil {
    response.Abort(ctx, http.StatusInternalServerError, err)
    return
}
...

if content != nil {
    // Данные найдены в кеше - пишем их в ответ
    response.SetStatusCode(http.StatusOK).Write(ctx, responses.JSONContentType, content)
    return
}
...

// Запрашиваем данные из базы данных и сериализуем их 
...

// Сохраняем данные в кеше
if err = h.userFeedCacheProvider.SetEntry(userID, content); err != nil {
    response.Abort(ctx, http.StatusInternalServerError, err)
    return
}
```

Как видно, лента новостей формируется на уровне обработчика, а не "на уровне кешей".
В кеше сохраняются не доменные объекты, а сериализованные данные. 
После удачного запроса из кеша данные сразу отправляются в ответ без дорогостоящей сериализации.

## Инвалидация кеша

Как указано выше, лента новостей формируется на уровне обработчика, а не "на уровне кешей".
Это освобождает кеш от логики построения ленты. Кеш умеет писать, читать и инвалидировать данные.

Пример работы с инвалидацией кеша можно посмотреть в [файле api/posts/handler.go](../api/feeds/handler.go).
После завершения операции создания, обновления или удаления сообщения производится инвалидация кешей пользователей, которые подписаны на автора сообщения.

```golang
...

if err := h.invalidateUserCaches(ctx, userID); err != nil {
    response.Abort(ctx, http.StatusInternalServerError, err)
    return
}

func (h *Handler) invalidateUserCaches(ctx context.Context, userID int64) error {
	// Запрашиваем фолловеров текущего пользователя
	followerIDs, err := h.followerRepo.GetFollowByList(ctx, userID, 0, 0)
	if err != nil {
		return err
	}

	// Инвалидируем кеш фолловеров
	h.userFeedCacheInvalidator.InvalidateEntries(followerIDs)
	return nil
}
```

Инвалидация ленты находиться в методе `batchUserCacheInvalidator.InvalidateEntries` в [файле api/caches/feed.go](../api/caches/feed.go).
Пользователи делятся на группы и для каждой планируется событие сброса кеша с увеличивающимся интервалом.

```golang
....

step++
batch := userIDs[start:end]

go func(batch []int64, delay time.Duration, cache *bigcache.BigCache) {
    select {
    case <-time.After(delay):
        for _, id := range batch {
            var key = prepareNewsFeedKey(id)
            err := cache.Delete(key)
            if err != nil && !errors.Is(err, bigcache.ErrEntryNotFound) {
                log.Printf("failed to invalidate cache entry (%s): %v\n", key, err)
            }
        }
    }
}(batch, time.Duration(step)*time.Minute, f.cache)
...
```
