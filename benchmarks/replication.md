# Реализация репликации базы данных и нагрузочное тестирование

## Репликация

По условию задачи требуется:
- кол-во реплик: `2`; 
- тип репликации: `Row-Based`;
- включен режим `GTID_MODE`;
- настроена полусинхронная репликация (`Semisynchronous Replication`).

Репликация баз данных реализована через docker контейнеры, подключённые к одной сети.
Версия `MySQL`: `8.0`.

### Сеть
```
$ docker network create --driver bridge isolated_tsn
```

### Master
Скрипт запуска контейнера:
```
$ sudo docker run \
--name=mysql8m \
--restart=on-failure \
--network=isolated_tsn \
--mount type=bind,src=/etc/mysql/tsn/master.cnf,dst=/etc/my.cnf \
--mount type=bind,src=/etc/mysql/tsn/secrets/user,dst=/etc/tsn.pwd \
--mount type=bind,src=/var/lib/mysql/tsn/master,dst=/var/lib/mysql \
--mount type=bind,src=/var/log/mysql/tsn,dst=/var/log/mysql \
--mount type=bind,src=/var/tsn/backups,dst=/var/backups \
--mount type=bind,src=/var/lib/mysql/tsn/migrations/seed.sql,dst=/docker-entrypoint-initdb.d/seed.sql \
-e MYSQL_USER="tsn" -e MYSQL_PASSWORD_FILE="/etc/tsn.pwd" -e MYSQL -e MYSQL_DATABASE="tiny_social_net" -e MYSQL_RANDOM_ROOT_PASSWORD="yes" -p 3306:3306 \
-d mysql/mysql-server:8.0 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```
Файл конфигурации `/etc/my.cnf`:
```
[mysqld]
server_id=100
gtid_mode=ON
enforce-gtid-consistency=ON
log_bin=/var/log/mysql/tsn-bin
log_replica_updates=OFF
replicate-do-db=tiny_social_net
sync_binlog=0
max_connections=1024
```
После запуска контейнера вычитываем из его лога автоматически сгенерированный пароль пользователя `root`:  
```
$ sudo docker logs mysql8m 2>&1 | grep "GENERATED ROOT PASSWORD"
```
Входим в контейнер запускаем mysql под пользователем `root` (его пароль добыт на предыдущем шаге):
```
$ sudo docker exec -it mysql8m mysql -uroot -p"<пароль>"
```
Создаём пользователя для выполнения репликации. С данным пользователем слейвы будут подключаться к мастеру:
```
mysql> CREATE USER 'replication'@'%';
mysql> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';
```
Устанавливаем плагин `rpl_semi_sync`, включаем `Semisynchronous Replication` и проверяем:
```
mysql> INSTALL PLUGIN rpl_semi_sync_source SONAME 'semisync_source.so';
mysql> SELECT PLUGIN_NAME, PLUGIN_STATUS FROM INFORMATION_SCHEMA.PLUGINS WHERE PLUGIN_NAME LIKE '%semi%';
+----------------------+---------------+
| PLUGIN_NAME          | PLUGIN_STATUS |
+----------------------+---------------+
| rpl_semi_sync_source | ACTIVE        |
+----------------------+---------------+
1 row in set (0.00 sec)

mysql> SET GLOBAL rpl_semi_sync_source_enabled = 1;
mysql> SET GLOBAL rpl_semi_sync_source_timeout = 500;
mysql> SHOW VARIABLES LIKE 'rpl_%';
+---------------------------------------------+------------+
| Variable_name                               | Value      |
+---------------------------------------------+------------+
| rpl_read_size                               | 8192       |
| rpl_semi_sync_source_enabled                | ON         |
| rpl_semi_sync_source_timeout                | 500        |
| rpl_semi_sync_source_trace_level            | 32         |
| rpl_semi_sync_source_wait_for_replica_count | 1          |
| rpl_semi_sync_source_wait_no_replica        | ON         |
| rpl_semi_sync_source_wait_point             | AFTER_SYNC |
| rpl_stop_replica_timeout                    | 31536000   |
| rpl_stop_slave_timeout                      | 31536000   |
+---------------------------------------------+------------+
9 rows in set (0.01 sec)

```
Блокируем базу на запись, запрашиваем текущее состояние, делаем дамп и снимаем блокировку.
Явно включаем `set-gtid-purged`, чтобы в дампе присутствовала установка переменной `GLOBAL.GTID_PURGED`.
Данные из `SHOW MASTER STATUS` будут нужны, чтобы установить начальную позицию на слейвах. 
```
mysql> FLUSH TABLES WITH READ LOCK;
mysql> SHOW MASTER STATUS;
mysql> SHOW MASTER STATUS\G
*************************** 1. row ***************************
             File: tsn-bin.000007
         Position: 643
     Binlog_Do_DB: 
 Binlog_Ignore_DB: 
Executed_Gtid_Set: 2fcf5fc4-0a79-11ed-8565-0242ac1d0002:1-2,
4ce9a8ec-0a7a-11ed-85fb-0242ac1d0002:1-479639,
81c0347d-0a75-11ed-85fb-0242ac1d0002:1-2,
a74a31c0-0a6d-11ed-8699-0242ac1d0002:1-2,
b0e56ebc-0ce9-11ed-8a2d-0242ac1d0002:1-2,
c67d8339-09d8-11ed-853d-0242ac1d0002:1-4
1 row in set (0.00 sec)

mysql> exit;

$ mysqldump --set-gtid-purged=ON tiny_social_net -uroot -p"<пароль>" > /var/backups/tsn.sql
$ mysql -uroot -p"<пароль>"
mysql> UNLOCK TABLES;
```

### Replica
Приводится сценарий для первой реплики. Для второй реплики сценарий аналогичен, меняются значения в файле конфигурации и имя контейнера. 
Скрипт запуска контейнера:
```
$ sudo docker run \
--name=mysql8s1 \
--restart=on-failure \
--network=isolated_tsn \
--mount type=bind,src=/etc/mysql/tsn/slave_1.cnf,dst=/etc/my.cnf \
--mount type=bind,src=/etc/mysql/tsn/secrets/user,dst=/etc/tsn.pwd \
--mount type=bind,src=/var/lib/mysql/tsn/slave1,dst=/var/lib/mysql \
--mount type=bind,src=/var/tsn/backups,dst=/var/backups \
--mount type=bind,src=/var/lib/mysql/tsn/migrations/seed.sql,dst=/docker-entrypoint-initdb.d/seed.sql \
-e MYSQL_USER="tsn" -e MYSQL_PASSWORD_FILE="/etc/tsn.pwd" -e MYSQL -e MYSQL_DATABASE="tiny_social_net" -e MYSQL_RANDOM_ROOT_PASSWORD="yes" -p 3316:3306 \
-d mysql/mysql-server:8.0 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci	
```
Файл конфигурации `/etc/my.cnf`:
```
[mysqld]
server_id=101
gtid_mode=ON
enforce-gtid-consistency=ON
max_connections=1024
```
После запуска контейнера вычитываем из его лога автоматически сгенерированный пароль пользователя `root`:
```
$ sudo docker logs mysql8s1 2>&1 | grep "GENERATED ROOT PASSWORD"
```
Входим в контейнер и загружаем дамп базы мастера:
```
$ docker exec -it mysql8s1 bash
$ mysql tiny_social_net -uroot -p"<пароль>" < /var/backups/tsn.sql
```
Устанавливаем плагин `rpl_semi_sync`, включаем `Semisynchronous Replication` и проверяем:
```
mysql -uroot -p"<пароль>"
mysql> INSTALL PLUGIN rpl_semi_sync_replica SONAME 'semisync_replica.so';
mysql> SELECT PLUGIN_NAME, PLUGIN_STATUS FROM INFORMATION_SCHEMA.PLUGINS WHERE PLUGIN_NAME LIKE '%semi%';
+-----------------------+---------------+
| PLUGIN_NAME           | PLUGIN_STATUS |
+-----------------------+---------------+
| rpl_semi_sync_replica | ACTIVE        |
+-----------------------+---------------+
1 row in set (0.00 sec)

mysql> SET GLOBAL rpl_semi_sync_replica_enabled = 1;
mysql> SHOW VARIABLES LIKE 'rpl_%';
+-----------------------------------+----------+
| Variable_name                     | Value    |
+-----------------------------------+----------+
| rpl_read_size                     | 8192     |
| rpl_semi_sync_replica_enabled     | ON       |
| rpl_semi_sync_replica_trace_level | 32       |
| rpl_stop_replica_timeout          | 31536000 |
| rpl_stop_slave_timeout            | 31536000 |
+-----------------------------------+----------+
5 rows in set (0.01 sec)
```
Привязываем реплику к мастеру, устанавливая имя хоста, имя файла логов, стартовую позицию и пользователя, который был предварительно создан на мастере.
Стартуем репликацию и проверяем. Ошибок нет - всё успешно запустилось.
```
mysql> CHANGE MASTER TO MASTER_HOST='mysql8m', MASTER_USER='replication', MASTER_LOG_FILE='tsn-bin.000007', MASTER_LOG_POS=643;
mysql> STOP REPLICA IO_THREAD;
mysql> START REPLICA IO_THREAD;
mysql> SHOW REPLICA STATUS\G
*************************** 1. row ***************************
Slave_IO_State: Waiting for source to send event
Master_Host: mysql8m
Master_User: replication
Master_Port: 3306
...
Slave_SQL_Running_State: Replica has read all relay log; waiting for more updates
...
```
## Нагрузочное тестирование
Цель: перевести тяжелые запросы чтения из базы на реплики. Оценить улучшение быстродействия сервиса.
### Что сделано в кодовой базе
1. Добавлен параметр конфигурации `TINY_SOCIAL_NET_REPLICA_DSN`, где можно указать доступные для сервиса реплики основной базы данных.
2. Реализован простой балансировщик нагрузки для запросов в базу данных `internal/database/balancers/balanсer.go`. Для равномерного распределения запросов на чтение между репликами применяется алгоритм `Round-Robin`.

### Результаты
В таблице приведены результаты нагрузочного тестирования.
Вызываемый метод: `/api/v2/user/find` - поиск пользователей по части имени и фамилии. 
Нагрузка равномерно распределялась балансировщиком на две реплики.
Тестирование проводилось при пуле 1024 соединения на каждой их реплик.

На `1 000 пользователей` квантиль 90% для времени ответа укладывается в `3 миллисекунды` - очень близко к практическому минимуму.
На `5 000 пользователей` квантиль 90% для времени ответа укладывается в `25 миллисекунд` - нормальное значение для продакшена.
На `10 000 пользователей` квантиль 90% для времени ответа укладывается в `30 миллисекунд` - видим только незначительную деградацию при увеличении нагрузки вдвое.

|  Users | Loops | Latency Min (ms) | Latency Max (ms) | Latency 0.9 (ms) | Latency 0.99 (ms) | Throughput (bytes/sec) |
|-------:|------:|-----------------:|-----------------:|-----------------:|------------------:|-----------------------:|
|  1 000 |     1 |                1 |                9 |                3 |                 6 |                   1000 |
|  5 000 |     1 |                2 |               64 |               23 |                44 |                   1783 |
| 10 000 |     1 |                2 |               80 |               29 |                56 |                   1813 |
| 20 000 |     1 |                2 |             1163 |               85 |               292 |                   2173 |

Ниже для сравнения приводятся результаты нагрузочного тестирования без репликации.
На `10 000 пользователей` по квантилю 90% улучшение более чем в 3 раза - `29 против 106 миллисекунд`.
При этом пропускная способность возросла почти в два раза - `1813 против 1093`.
На `10 000 пользователей` по квантилю 99% улучшение более чем в 20 раз - `56 против 1191 миллисекунд`.
Сервис работает стабильно и по квантилю 90% время ответа укладывается в `30 миллисекунд` - приемлемое значение для продакшена.
Появилась возможность работать с `20 000 пользователями`. При этом по квантилю 90% время ответа укладывается в `90 миллисекунд`.
Это приемлемое значение только для пиковых нагрузок. Далее из таблицы статистики утилизации ресурсов `docker` контейнерами будет видно, что мы ограничены по `CPU`.

|  Users | Loops | Latency Min (ms) | Latency Max (ms) | Latency 0.9 (ms) | Latency 0.99 (ms) | Throughput (bytes/sec) |
|-------:|------:|-----------------:|-----------------:|-----------------:|------------------:|-----------------------:|
|  1 000 |     1 |                5 |               92 |               47 |                67 |                    977 |
| 10 000 |     1 |                4 |             2075 |              106 |              1191 |                   1093 |

В таблице приведена статистика утилизации ресурсов `docker` контейнерами баз данных под нагрузкой `20 000 пользователей` на метод `/api/v2/user/find`.
- `mysql8m`: контейнер мастера;
- `mysql8s1`: контейнер первой реплики;
- `mysql8s2`: контейнер второй реплики.

| CONTAINER ID | NAME     |  CPU % |   MEM USAGE / LIMIT | MEM % |         NET I/O |     BLOCK I/O |
|:-------------|:---------|-------:|--------------------:|------:|----------------:|--------------:|
| 39f285777b66 | mysql8m  |   0.11 | 650.1MiB / 31.11GiB |  2.04 | 27.7kB / 17.4kB |  331MB / 14MB |
| 6cb841b845ce | mysql8s1 | 301.99 |   783MiB / 31.11GiB |  2.46 | 84.1MB / 1.04GB | 68.3MB / 16MB |
| b120fe5f6722 | mysql8s2 | 292.54 |   788MiB / 31.11GiB |  2.47 | 83.3MB / 1.04GB | 71.7MB / 16MB |

Локально сервис с двумя репликами держит `1 500 RPS`.
Видим по потреблению `CPU`, что мастер простаивает, а реплики полностью загружают по 3 ядра `CPU`.
Показатель `RPS` вырос в 3 раза по сравнению с вариантом без репликации.
Дальнейшее увеличение быстродействия упирается только в ограничения по `CPU`.

График нагрузки `/api/v2/user/find`
![user-find-rps](./transactions-user-find.png "График нагрузки /api/v2/user/find")
## Эмуляция отключения мастера и повышение реплики до мастера
Стартуем процесс записи в базу мастера (запуск скрипта импорта).
Останавливаем контейнер мастера:
```
$ sudo docker kill --signal=9  mysql8m
```

### Повышение первой реплики до мастера
Входим в контейнер:
```
$ sudo docker exec -it mysql8s1 mysql -uroot -p"<пароль>"
```

Запоминаем позицию, где остановилась репликация: поля `Master_Log_File` и `Read_Master_Log_Pos`
Если значение в поле `Exec_Master_Log_Pos` меньше чем `Read_Master_Log_Pos`, то ждём пока реплика догонит мастер.
```
mysql> SHOW SLAVE STATUS\G
*************************** 1. row ***************************
               Slave_IO_State: Reconnecting after a failed source event read
                  Master_Host: mysql8m
                  Master_User: replication
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: tsn-bin.000007
          Read_Master_Log_Pos: 446162
               Relay_Log_File: 27c0a37b02f3-relay-bin.000003
                Relay_Log_Pos: 445843
        Relay_Master_Log_File: tsn-bin.000007
      
          ...
          Exec_Master_Log_Pos: 446162
```
Повышение реплики до мастера:
```
mysql> STOP REPLICA IO_THREAD;
mysql> SET GLOBAL rpl_semi_sync_replica_enabled = 0;

mysql> INSTALL PLUGIN rpl_semi_sync_source SONAME 'semisync_source.so';
mysql> SET GLOBAL rpl_semi_sync_source_enabled = 1;
mysql> SET GLOBAL rpl_semi_sync_source_timeout = 500;
mysql> CREATE USER 'replication'@'%';
mysql> GRANT REPLICATION SLAVE ON *.* TO 'replication'@'%';

mysql> FLUSH TABLES WITH READ LOCK;
mysql> RESET MASTER;
mysql> SHOW MASTER STATUS;
+---------------+----------+--------------+------------------+-------------------+
| File          | Position | Binlog_Do_DB | Binlog_Ignore_DB | Executed_Gtid_Set |
+---------------+----------+--------------+------------------+-------------------+
| binlog.000001 |      157 |              |                  |                   |
+---------------+----------+--------------+------------------+-------------------+
1 row in set (0.00 sec)

mysql> UNLOCK TABLES;
```

### Переключение второй реплики на новый мастер
Входим в контейнер:
```
$ sudo docker exec -it mysql8s2 mysql -uroot -p"<пароль>"
```

Переключение реплики до мастера:
```
mysql> STOP REPLICA IO_THREAD;
mysql> CHANGE MASTER TO MASTER_HOST='mysql8s1', MASTER_USER='replication', MASTER_LOG_FILE='binlog.000001', MASTER_LOG_POS=157;
mysql> START REPLICA IO_THREAD;
mysql> SHOW REPLICA STATUS\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for source to send event
                  Master_Host: mysql8s1
                  Master_User: replication
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: binlog.000001
          Read_Master_Log_Pos: 157
          ...
```
### Проверка потерянных записей логов
Запускаем старый мастер заходим в контейнер и сравниваем позицию лога с позицией на репликах.

```
mysql> SHOW BINARY LOGS;
+----------------+-----------+-----------+
| Log_name       | File_size | Encrypted |
+----------------+-----------+-----------+
| tsn-bin.000001 |       180 | No        |
| tsn-bin.000002 |       157 | No        |
| tsn-bin.000003 |       180 | No        |
| tsn-bin.000004 |       180 | No        |
| tsn-bin.000005 |       180 | No        |
| tsn-bin.000006 |       180 | No        |
| tsn-bin.000007 |    446162 | No        |
| tsn-bin.000008 |       197 | No        |
+----------------+-----------+-----------+
```
На репликах значения были следующие значения:
```
Master_Log_File: tsn-bin.000007
Read_Master_Log_Pos: 446162
```
Позиция в логе `tsn-bin.000007` совпадает - этот лог обработан репликами полностью.
При остановке сервера в лог пишется событие `Stop` и при старте начинается новый лог. Нормально, что есть новый файл логов `tsn-bin.000008`.
Выводим события нового лога и проверяем:
```
mysql> SHOW BINLOG EVENTS IN 'tsn-bin.000008' FROM 0;
+----------------+-----+----------------+-----------+-------------+--------------------------------------------+
| Log_name       | Pos | Event_type     | Server_id | End_log_pos | Info                                       |
+----------------+-----+----------------+-----------+-------------+--------------------------------------------+
| tsn-bin.000008 |   4 | Format_desc    |       100 |         126 | Server ver: 8.0.29, Binlog ver: 4          |
| tsn-bin.000008 | 126 | Previous_gtids |       100 |         197 | b0e56ebc-0ce9-11ed-8a2d-0242ac1d0002:1-863 |
+----------------+-----+----------------+-----------+-------------+--------------------------------------------+
2 rows in set (0.00 sec)
```

Событий `WRITE_ROWS`, `UPDATE_ROWS`, `DELETE_ROWS` нет. Потерянных транзакций нет.