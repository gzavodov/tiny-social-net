CREATE TABLE `users`
(
    `id`            BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `login`         VARCHAR(255) NOT NULL,
    `firstName`     VARCHAR(64)  NULL,
    `lastName`      VARCHAR(64)  NULL,
    `email`         VARCHAR(255) NOT NULL,
    `password`      VARCHAR(255) NOT NULL,
    `residence`     VARCHAR(255) NULL,
    `dayOfBirth`    DATE         NULL,
    `gender`        CHAR(1)      NULL,
    `bio`           TEXT         NOT NULL,
    `joined`        VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE UNIQUE INDEX `idx_users_login` ON `users`(`login`);
CREATE INDEX `idx_users_last_first_name` ON `users`(`lastName`, `firstName`);
CREATE INDEX `idx_users_first_last_name` ON `users`(`firstName`, `lastName`);

CREATE TABLE `profile_views`
(
    `id`       BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `viewBy`   BIGINT       NOT NULL,
    `viewTo`   BIGINT       NOT NULL,
    `viewTime` VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `follow`
(
    `id`         BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `followBy`   BIGINT       NOT NULL,
    `followTo`   BIGINT       NOT NULL,
    `followTime` VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;


CREATE TABLE `posts`
(
    `id`        BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title`     VARCHAR(255) NOT NULL,
    `content`   TEXT         NOT NULL,
    `createdBy` BIGINT       NOT NULL,
    `createdAt` VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE `likes`
(
    `id`       BIGINT       NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `postId`   BIGINT       NOT NULL,
    `likeBy`   BIGINT       NOT NULL,
    `likeTime` VARCHAR(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
