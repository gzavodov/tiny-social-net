package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type FollowerRepository struct {
	db *sqlx.DB
}

func NewFollowerRepository(db *sqlx.DB) *FollowerRepository {
	return &FollowerRepository{db: db}
}

func (r *FollowerRepository) Create(ctx context.Context, followBy, followTo int64, followTime time.Time) error {
	_, err := r.db.ExecContext(ctx,
		`INSERT INTO follow(followBy, followTo, followTime) VALUES(?, ?, ?)`,
		followBy,
		followTo,
		followTime.Truncate(time.Microsecond),
	)

	if err != nil {
		return fmt.Errorf("failed to create follower: %w", err)
	}

	return nil
}

func (r *FollowerRepository) Delete(ctx context.Context, followBy, followTo int64) error {
	_, err := r.db.ExecContext(ctx, `DELETE FROM follow WHERE followBy = ? AND followTo = ?`, followBy, followTo)
	if err != nil {
		return fmt.Errorf("failed to delete follow relation: %w", err)
	}

	return nil
}

func (r *FollowerRepository) IsExists(ctx context.Context, followBy, followTo int64) (bool, error) {
	row := r.db.QueryRowxContext(
		ctx,
		`SELECT 1 FROM follow WHERE followBy = ? AND followTo = ? LIMIT 1`,
		followBy,
		followTo,
	)

	if row.Err() != nil {
		return false, fmt.Errorf("unable to check if follower exists: failed to select data: %w", row.Err())
	}

	if err := row.Scan(new(uint8)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("unable to check if if follower exists: failed to scan column: %w", err)
	}

	return true, nil
}

func (r *FollowerRepository) GetFollowToList(ctx context.Context, followBy int64, limit int, offset int) ([]int64, error) {
	if limit <= 0 {
		limit = DefaultLimit
	}

	if offset < 0 {
		offset = 0
	}

	rows, err := r.db.QueryxContext(ctx,
		`SELECT followTo FROM follow WHERE followBy = ? LIMIT ? OFFSET ?`,
		followBy,
		limit,
		offset,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select followers: %w", err)
	}

	list := make([]int64, 0, limit)
	for rows.Next() {
		var id int64
		if err = rows.Scan(&id); err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				return nil, nil
			}

			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		list = append(list, id)
	}

	return list, nil
}

func (r *FollowerRepository) GetFollowToCount(ctx context.Context, userIDs []int64) (map[int64]int64, error) {
	if len(userIDs) == 0 {
		return nil, nil
	}

	query, args, err := sqlx.In(`SELECT followBy AS userId, COUNT(*) AS followToQty FROM follow WHERE followBy IN(?) GROUP BY followBy`, userIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare select query: %w", err)
	}

	//query = r.db.Rebind(query)
	rows, err := r.db.QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select followers: %w", err)
	}

	var userID, followToQty int64
	results := make(map[int64]int64, len(userIDs))
	for rows.Next() {
		err := rows.Scan(&userID, &followToQty)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				break
			}

			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		results[userID] = followToQty
	}

	return results, nil
}

func (r *FollowerRepository) GetFollowByList(ctx context.Context, followTo int64, limit int, offset int) ([]int64, error) {
	if limit <= 0 {
		limit = DefaultLimit
	}

	if offset < 0 {
		offset = 0
	}

	rows, err := r.db.QueryxContext(ctx,
		`SELECT followBy FROM follow WHERE followTo = ? LIMIT ? OFFSET ?`,
		followTo,
		limit,
		offset,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select followers list: %w", err)
	}

	list := make([]int64, 0, limit)
	for rows.Next() {
		var id int64
		if err = rows.Scan(&id); err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				return nil, nil
			}

			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		list = append(list, id)
	}

	return list, nil
}

func (r *FollowerRepository) GetFollowByCount(ctx context.Context, userIDs []int64) (map[int64]int64, error) {
	if len(userIDs) == 0 {
		return nil, nil
	}

	query, args, err := sqlx.In(`SELECT followTo AS userId, COUNT(*) AS followByQty FROM follow WHERE followTo IN(?) GROUP BY followTo`, userIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare select query: %w", err)
	}

	//query = r.db.Rebind(query)
	rows, err := r.db.QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select followers: %w", err)
	}

	var userID, followByQty int64
	results := make(map[int64]int64, len(userIDs))
	for rows.Next() {
		err := rows.Scan(&userID, &followByQty)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				break
			}

			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		results[userID] = followByQty
	}

	return results, nil
}
