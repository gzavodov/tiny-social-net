package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/internal/database/balancers"

	"github.com/go-sql-driver/mysql"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

const (
	DefaultLimit      int = 1000
	InitialListLength     = 100
)

type UserRepository struct {
	balancer *balancers.LoadBalancer
}

func NewUserRepository(balancer *balancers.LoadBalancer) *UserRepository {
	return &UserRepository{balancer: balancer}
}

type RowScanner interface {
	Columns() ([]string, error)
	Scan(dest ...interface{}) error
	MapScan(dest map[string]interface{}) error
}

func (r *UserRepository) Create(ctx context.Context, user *domain.User) error {
	result, err := r.balancer.GetWriteConnection().ExecContext(ctx,
		`INSERT INTO users (login, firstName, lastName, email, password, residence, dayOfBirth, gender, bio, joined) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		user.Login,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Password,
		user.Residence,
		toNullSqlDate(user.DayOfBirth),
		toNullSqlString(user.Gender.String()),
		user.Bio,
		user.Joined.Truncate(time.Microsecond).Truncate(time.Nanosecond),
	)

	if err != nil {
		if sqlErr, ok := err.(*mysql.MySQLError); ok && sqlErr.Number == 1062 {
			return domain.ErrDuplicateRecord
		}

		return fmt.Errorf("failed to create user: %w", err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		return fmt.Errorf("failed to get user ID: %w", err)
	}

	user.ID = id
	return nil
}

func (r *UserRepository) Get(ctx context.Context, id int64) (*domain.User, error) {
	row := r.balancer.GetReadConnection().QueryRowxContext(ctx,
		`SELECT id, login, firstName, lastName, email, password, residence, dayOfBirth, gender, bio, joined FROM users WHERE id = ? LIMIT 1`,
		id,
	)

	if row.Err() != nil {
		return nil, fmt.Errorf("failed to select user: %w", row.Err())
	}

	user, err := scanUser(row, true)
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}
	return user, nil
}

func (r *UserRepository) GetByLogin(ctx context.Context, login string) (*domain.User, error) {
	row := r.balancer.GetReadConnection().QueryRowxContext(ctx,
		`SELECT id, login, firstName, lastName, email, password, residence, dayOfBirth, gender, bio, joined FROM users WHERE login = ? LIMIT 1`,
		login,
	)

	if row.Err() != nil {
		return nil, fmt.Errorf("failed to select user: %w", row.Err())
	}

	user, err := scanUser(row, true)
	if err != nil {
		return nil, fmt.Errorf("failed to get user: %w", err)
	}
	return user, nil
}

func (r *UserRepository) Update(ctx context.Context, user *domain.User) error {
	_, err := r.balancer.GetWriteConnection().ExecContext(ctx,
		`UPDATE users SET login = ?, firstName = ?, lastName = ?, email = ?, password = ?, residence = ?, dayOfBirth = ?, gender = ?, bio = ?, joined = ? WHERE id = ?`,
		user.Login,
		user.FirstName,
		user.LastName,
		user.Email,
		user.Password,
		user.Residence,
		toNullSqlDate(user.DayOfBirth),
		toNullSqlString(user.Gender.String()),
		user.Bio,
		user.Joined.Truncate(time.Microsecond),
		user.ID,
	)

	if err != nil {
		return fmt.Errorf("failed to update user: %w", err)
	}

	return nil
}

func (r *UserRepository) Delete(ctx context.Context, id int64) error {
	_, err := r.balancer.GetWriteConnection().ExecContext(ctx, `DELETE FROM users WHERE id = ?`, id)
	if err != nil {
		return fmt.Errorf("failed to delete user: %w", err)
	}

	return nil
}

func (r *UserRepository) IsEmailAddressExists(ctx context.Context, email string) (bool, error) {
	row := r.balancer.GetReadConnection().QueryRowxContext(ctx, `SELECT 1 FROM users WHERE email = ? LIMIT 1`, email)

	if row.Err() != nil {
		return false, fmt.Errorf("unable to check if email address exists: failed to select data: %w", row.Err())
	}

	if err := row.Scan(new(uint8)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("unable to check if email address exists: failed to scan column: %w", err)
	}

	return true, nil
}

func (r *UserRepository) IsExists(ctx context.Context, id int64) (bool, error) {
	row := r.balancer.GetReadConnection().QueryRowxContext(ctx, `SELECT 1 FROM users WHERE id = ? LIMIT 1`, id)

	if row.Err() != nil {
		return false, fmt.Errorf("unable to check if user exists: failed to select data: %w", row.Err())
	}

	if err := row.Scan(new(uint8)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("unable to check if user exists: failed to scan column: %w", err)
	}

	return true, nil
}

func (r *UserRepository) IsLoginExists(ctx context.Context, login string) (bool, error) {
	row := r.balancer.GetReadConnection().QueryRowxContext(ctx, `SELECT 1 FROM users WHERE login = ? LIMIT 1`, login)

	if row.Err() != nil {
		return false, fmt.Errorf("unable to check if login exists: failed to select data: %w", row.Err())
	}

	if err := row.Scan(new(uint8)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("unable to check if login exists: failed to scan column: %w", err)
	}

	return true, nil
}

func (r *UserRepository) GetList(ctx context.Context, filter *domain.Filter, limit int, offset int) ([]*domain.User, error) {
	initListLength := limit
	if limit <= 0 {
		limit = DefaultLimit
		initListLength = InitialListLength
	}

	if offset < 0 {
		offset = 0
	}

	var (
		query string
		args  []interface{}
	)

	expression, args := prepareFilterExpression(filter)
	if len(expression) == 0 {
		query = "SELECT `id`, `login`, `firstName`, `lastName`, `email`, `residence`, `dayOfBirth`, `gender`, `bio`, `joined` FROM `users` ORDER BY `id` ASC LIMIT ? OFFSET ?"
		args = []interface{}{limit, offset}
	} else {
		query = fmt.Sprintf(
			"SELECT `id`, `login`, `firstName`, `lastName`, `email`, `residence`, `dayOfBirth`, `gender`, `bio`, `joined` FROM `users` WHERE %s ORDER BY `id` ASC LIMIT ? OFFSET ?",
			expression,
		)

		args = append(args, []interface{}{limit, offset}...)
	}

	rows, err := r.balancer.GetReadConnection().QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select users: %w", err)
	}

	list := make([]*domain.User, 0, initListLength)
	for rows.Next() {
		user, err := scanUser(rows, false)
		if err != nil {
			return nil, fmt.Errorf("failed to get user: %w", err)
		}

		list = append(list, user)
	}

	return list, nil
}

func (r *UserRepository) GetListByIDs(ctx context.Context, userIDs []int64) ([]*domain.User, error) {
	if len(userIDs) == 0 {
		return nil, nil
	}

	query, args, err := sqlx.In(`SELECT id, login, firstName, lastName, email, residence, dayOfBirth, gender, bio, joined FROM users WHERE id IN(?) ORDER BY id`, userIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare select query: %w", err)
	}

	rows, err := r.balancer.GetReadConnection().QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select users: %w", err)
	}

	list := make([]*domain.User, 0, len(userIDs))
	for rows.Next() {
		user, err := scanUser(rows, false)
		if err != nil {
			return nil, fmt.Errorf("failed to get user: %w", err)
		}

		list = append(list, user)
	}

	return list, nil
}

func (r *UserRepository) GetLoginByIDs(ctx context.Context, userIDs []int64) (map[int64]string, error) {
	if len(userIDs) == 0 {
		return nil, nil
	}

	query, args, err := sqlx.In(`SELECT id, login FROM users WHERE id IN(?) ORDER BY id`, userIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare select query: %w", err)
	}

	rows, err := r.balancer.GetReadConnection().QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select users: %w", err)
	}

	var (
		id    int64
		login string
	)

	results := make(map[int64]string, len(userIDs))
	for rows.Next() {
		if err = rows.Scan(&id, &login); err != nil {
			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		results[id] = login
	}

	return results, nil
}

func scanUser(row RowScanner, enablePassword bool) (*domain.User, error) {
	var (
		id                                     int64
		login, email, password, bio            string
		firstName, lastName, residence, gender sql.NullString
		joinedRaw                              rawTime
		dayOfBirth                             sql.NullTime
		err                                    error
	)

	if enablePassword {
		err = row.Scan(&id, &login, &firstName, &lastName, &email, &password, &residence, &dayOfBirth, &gender, &bio, &joinedRaw)
	} else {
		err = row.Scan(&id, &login, &firstName, &lastName, &email, &residence, &dayOfBirth, &gender, &bio, &joinedRaw)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, fmt.Errorf("failed to scan row: %w", err)
	}

	joined, err := joinedRaw.DateTime()
	if err != nil {
		return nil, fmt.Errorf("failed to parse time (%s): %w", joinedRaw.String(), err)
	}

	return &domain.User{
		ID:         id,
		Login:      login,
		FirstName:  firstName.String,
		LastName:   lastName.String,
		Email:      email,
		Password:   password,
		Residence:  residence.String,
		DayOfBirth: dayOfBirth.Time,
		Gender:     domain.Gender(gender.String),
		Bio:        bio,
		Joined:     joined,
	}, nil
}
