package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type ProfileViewRepository struct {
	db *sqlx.DB
}

func NewProfileViewRepository(db *sqlx.DB) *ProfileViewRepository {
	return &ProfileViewRepository{db: db}
}

func (r *ProfileViewRepository) Create(ctx context.Context, viewBy int64, viewTo int64, viewTime time.Time) error {
	_, err := r.db.ExecContext(ctx,
		`INSERT INTO profile_views(viewBy, viewTo, viewTime) VALUES(?, ?, ?)`,
		viewBy,
		viewTo,
		viewTime.Truncate(time.Microsecond),
	)

	if err != nil {
		return fmt.Errorf("failed to create profile view: %w", err)
	}

	return nil
}

func (r *ProfileViewRepository) GetCount(ctx context.Context, userIDs []int64) (map[int64]int64, error) {
	if len(userIDs) == 0 {
		return nil, nil
	}

	query, args, err := sqlx.In(`SELECT viewTo AS id, COUNT(*) AS qty FROM profile_views WHERE viewTo IN(?) GROUP BY viewTo`, userIDs)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare select query: %w", err)
	}

	//query = r.db.Rebind(query)
	rows, err := r.db.QueryxContext(ctx,
		query,
		args...,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select profile views: %w", err)
	}

	var id, qty int64
	results := make(map[int64]int64, len(userIDs))
	for rows.Next() {
		err := rows.Scan(&id, &qty)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				break
			}

			return nil, fmt.Errorf("failed to scan row: %w", err)
		}

		results[id] = qty
	}

	return results, nil
}
