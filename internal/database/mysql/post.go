package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type PostRepository struct {
	db *sqlx.DB
}

func NewPostRepository(db *sqlx.DB) *PostRepository {
	return &PostRepository{db: db}
}

func (r *PostRepository) Create(ctx context.Context, post *domain.Post) error {
	result, err := r.db.ExecContext(ctx,
		`INSERT INTO posts(title, content, createdBy, createdAt) VALUES (?, ?, ?, ?)`,
		post.Title,
		post.Content,
		post.CreatedBy,
		post.CreatedAt.Truncate(time.Microsecond),
	)

	if err != nil {
		return fmt.Errorf("failed to create post: %w", err)
	}

	id, err := result.LastInsertId()
	if err != nil {
		return fmt.Errorf("failed to get post ID: %w", err)
	}

	post.ID = id
	return nil
}

func (r *PostRepository) Get(ctx context.Context, id int64) (*domain.Post, error) {
	row := r.db.QueryRowxContext(ctx,
		`SELECT id, title, content, createdBy, createdAt FROM posts WHERE id = ? LIMIT 1`,
		id,
	)

	if row.Err() != nil {
		return nil, fmt.Errorf("failed to select post: %w", row.Err())
	}

	post, err := scanPost(row)
	if err != nil {
		return nil, fmt.Errorf("failed to get post: %w", err)
	}
	return post, nil
}

func (r *PostRepository) Update(ctx context.Context, post *domain.Post) error {
	_, err := r.db.ExecContext(ctx,
		`UPDATE posts SET title = ?, content = ? WHERE id = ?`,
		post.Title,
		post.Content,
		post.ID,
	)

	if err != nil {
		return fmt.Errorf("failed to update post: %w", err)
	}

	return nil
}

func (r *PostRepository) Delete(ctx context.Context, id int64) error {
	_, err := r.db.ExecContext(ctx, `DELETE FROM posts WHERE id = ?`, id)
	if err != nil {
		return fmt.Errorf("failed to delete post: %w", err)
	}

	return nil
}

func (r *PostRepository) GetList(ctx context.Context, createdBy int64, limit int, offset int) ([]*domain.Post, error) {
	initListLength := limit
	if limit <= 0 {
		limit = DefaultLimit
		initListLength = InitialListLength
	}

	if offset < 0 {
		offset = 0
	}

	var (
		rows *sqlx.Rows
		err  error
	)

	if createdBy > 0 {
		rows, err = r.db.QueryxContext(
			ctx,
			`SELECT id, title, content, createdBy, createdAt FROM posts WHERE createdBy=? ORDER BY id DESC LIMIT ? OFFSET ?`,
			createdBy,
			limit,
			offset,
		)
	} else {
		rows, err = r.db.QueryxContext(
			ctx,
			`SELECT id, title, content, createdBy, createdAt FROM posts ORDER BY id DESC LIMIT ? OFFSET ?`,
			limit,
			offset,
		)
	}

	if err != nil {
		return nil, fmt.Errorf("failed to select posts: %w", err)
	}

	list := make([]*domain.Post, 0, initListLength)
	for rows.Next() {
		user, err := scanPost(rows)
		if err != nil {
			return nil, fmt.Errorf("failed to get post list: %w", err)
		}

		list = append(list, user)
	}

	return list, nil
}

func (r *PostRepository) GetFollowingList(ctx context.Context, followBy int64, limit int, offset int) ([]*domain.Post, error) {
	initListLength := limit
	if limit <= 0 {
		limit = DefaultLimit
		initListLength = InitialListLength
	}

	if offset < 0 {
		offset = 0
	}

	rows, err := r.db.QueryxContext(
		ctx,
		`SELECT p.id, p.title, p.content, p.createdBy, p.createdAt FROM posts p INNER JOIN follow f ON f.followBy = ? AND f.followTo = p.createdBy ORDER BY p.id DESC LIMIT ? OFFSET ?`,
		followBy,
		limit,
		offset,
	)

	if err != nil {
		return nil, fmt.Errorf("failed to select posts: %w", err)
	}

	list := make([]*domain.Post, 0, initListLength)
	for rows.Next() {
		user, err := scanPost(rows)
		if err != nil {
			return nil, fmt.Errorf("failed to get post: %w", err)
		}

		list = append(list, user)
	}

	return list, nil
}

func scanPost(row RowScanner) (*domain.Post, error) {
	var id, createdBy int64
	var title, content string
	var createdAtRaw rawTime
	var err error

	if err := row.Scan(&id, &title, &content, &createdBy, &createdAtRaw); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}

		return nil, fmt.Errorf("failed to scan row: %w", err)
	}

	createdAt, err := createdAtRaw.DateTime()
	if err != nil {
		return nil, fmt.Errorf("failed to parse time (%s): %w", createdAtRaw.String(), err)
	}

	return &domain.Post{ID: id, Title: title, Content: content, CreatedBy: createdBy, CreatedAt: createdAt}, nil
}
