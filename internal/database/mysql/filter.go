package mysql

import (
	"fmt"
	"strings"

	"gitlab.com/gzavodov/tiny-social-net/domain"
)

func prepareFilterExpression(filter *domain.Filter) (string, []interface{}) {
	if filter == nil {
		return "", nil
	}

	expressions := make([]string, 0, len(filter.Fields))
	args := make([]interface{}, 0, 2*len(filter.Fields))
	for _, field := range filter.Fields {
		var condition string

		switch field.Operator {
		case domain.FilterOperatorStartsWith:
			condition = fmt.Sprintf("`%s` LIKE CONCAT(?, '%%')", field.Name)
		case domain.FilterOperatorLessThan:
			condition = fmt.Sprintf("`%s` < ?", field.Name)
		case domain.FilterOperatorLessOrEqual:
			condition = fmt.Sprintf("`%s` <= ?", field.Name)
		case domain.FilterOperatorGreaterThan:
			condition = fmt.Sprintf("`%s` > ?", field.Name)
		case domain.FilterOperatorGreaterOrEqual:
			condition = fmt.Sprintf("`%s` >= ?", field.Name)
		default:
			condition = fmt.Sprintf("`%s` = ?", field.Name)
		}

		expressions = append(expressions, condition)
		args = append(args, field.Value)
	}

	return strings.Join(expressions, fmt.Sprintf(" %s ", filter.Logic)), args
}
