package mysql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type LikeRepository struct {
	db *sqlx.DB
}

func NewLikeRepository(db *sqlx.DB) *LikeRepository {
	return &LikeRepository{db: db}
}

func (r *LikeRepository) Like(ctx context.Context, postID int64, likeBy int64, likeTime time.Time) error {
	_, err := r.db.ExecContext(ctx,
		`INSERT INTO likes(postId, likeBy, likeTime) VALUES (?, ?, ?)`,
		postID,
		likeBy,
		likeTime.Truncate(time.Microsecond),
	)

	if err != nil {
		return fmt.Errorf("failed to insert like: %w", err)
	}

	return nil
}

func (r *LikeRepository) UnLike(ctx context.Context, postID int64, likeBy int64) error {
	_, err := r.db.ExecContext(ctx,
		`DELETE FROM likes WHERE postId = ? AND likeBy = ?`,
		postID,
		likeBy,
	)

	if err != nil {
		return fmt.Errorf("failed to delete like: %w", err)
	}

	return nil
}

func (r *LikeRepository) IsLiked(ctx context.Context, postID int64, likeBy int64) (bool, error) {
	row := r.db.QueryRowxContext(
		ctx,
		`SELECT 1 FROM likes WHERE postId = ? AND likeBy = ? LIMIT 1`,
		postID,
		likeBy,
	)

	if row.Err() != nil {
		return false, fmt.Errorf("unable to check if like exists: failed to select data: %w", row.Err())
	}

	if err := row.Scan(new(uint8)); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return false, nil
		}

		return false, fmt.Errorf("unable to check if like exists: failed to scan column: %w", err)
	}

	return true, nil
}

func (r *LikeRepository) GetCount(ctx context.Context, postID int64) (uint32, error) {
	row := r.db.QueryRowxContext(
		ctx,
		`SELECT COUNT(*) qty FROM likes WHERE postId = ?`,
		postID,
	)

	if row.Err() != nil {
		return 0, fmt.Errorf("unable to get likes count: failed to select data: %w", row.Err())
	}

	var qty uint32
	if err := row.Scan(&qty); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return 0, nil
		}

		return 0, fmt.Errorf("unable to get likes count: failed to scan column: %w", err)
	}

	return qty, nil
}
