package mysql

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"gitlab.com/gzavodov/tiny-social-net/config"
)

func TestUserRepository_CRUD(t *testing.T) {
	ctx := context.Background()

	now := time.Now().UTC()
	joined := time.Date(now.Year(), now.Month(), now.Day(), 12, 0, 0, 0, time.UTC)

	conn := config.MustConnectToDatabase()
	defer conn.Close()

	repo := NewUserRepository(conn)

	testCases := []struct {
		createUser  *domain.User
		createError error
		updateUser  *domain.User
		updateError error
	}{
		{
			createUser: &domain.User{
				Login:    "test",
				Email:    "test@example.org",
				Password: "test",
				Joined:   joined,
			},
			updateUser: &domain.User{
				Login:    "test",
				Email:    "test@example.org",
				Password: "test",
				Bio: `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt 
					ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco 
					laboris nisi ut aliquip ex ea commodo consequat.`,
				Joined: joined,
			},
		},
	}

	for _, testCase := range testCases {
		createErr := repo.Create(ctx, testCase.createUser)
		if testCase.createError != nil {
			require.ErrorIs(t, createErr, testCase.createError)
			continue
		}

		require.NoError(t, createErr)
		require.Greater(t, testCase.createUser.ID, int64(0))

		user, getErr := repo.Get(ctx, testCase.createUser.ID)
		require.NoError(t, getErr)
		require.Equal(t, testCase.createUser, user)

		testCase.updateUser.ID = testCase.createUser.ID
		updateErr := repo.Update(ctx, testCase.updateUser)
		if testCase.updateError != nil {
			require.ErrorIs(t, updateErr, testCase.updateError)
		} else {
			user, getErr = repo.Get(ctx, testCase.updateUser.ID)
			require.NoError(t, getErr)
			require.Equal(t, testCase.updateUser, user)
		}

		require.NoError(t, repo.Delete(ctx, user.ID))
	}
}

func TestUserRepository_IsEmailAddressExists(t *testing.T) {
	ctx := context.Background()

	now := time.Now().UTC()
	joined := time.Date(now.Year(), now.Month(), now.Day(), 12, 0, 0, 0, time.UTC)

	conn := config.MustConnectToDatabase()
	defer conn.Close()

	repo := NewUserRepository(conn)

	testCases := []struct {
		user           *domain.User
		email          string
		expectedResult bool
	}{
		{
			user: &domain.User{
				Login:    "test1",
				Email:    "test1@example.org",
				Password: "test1",
				Joined:   joined,
			},
			email:          "test1@example.org",
			expectedResult: true,
		},
		{
			user: &domain.User{
				Login:    "test2",
				Email:    "test2@example.org",
				Password: "test2",
				Joined:   joined,
			},
			email:          "test200@example.org",
			expectedResult: false,
		},
	}

	for _, testCase := range testCases {
		require.NoError(t, repo.Create(ctx, testCase.user))
		require.Greater(t, testCase.user.ID, int64(0))

		actualResult, actualErr := repo.IsEmailAddressExists(ctx, testCase.email)
		require.NoError(t, actualErr)

		require.Equal(t, testCase.expectedResult, actualResult)

		require.NoError(t, repo.Delete(ctx, testCase.user.ID))
	}
}

func TestUserRepository_IsLoginExists(t *testing.T) {
	ctx := context.Background()

	now := time.Now().UTC()
	joined := time.Date(now.Year(), now.Month(), now.Day(), 12, 0, 0, 0, time.UTC)

	conn := config.MustConnectToDatabase()
	defer conn.Close()

	repo := NewUserRepository(conn)

	testCases := []struct {
		user           *domain.User
		login          string
		expectedResult bool
	}{
		{
			user: &domain.User{
				Login:    "test1",
				Email:    "test1@example.org",
				Password: "test1",
				Joined:   joined,
			},
			login:          "test1",
			expectedResult: true,
		},
		{
			user: &domain.User{
				Login:    "test2",
				Email:    "test2@example.org",
				Password: "test2",
				Joined:   joined,
			},
			login:          "test200",
			expectedResult: false,
		},
	}

	for _, testCase := range testCases {
		require.NoError(t, repo.Create(ctx, testCase.user))
		require.Greater(t, testCase.user.ID, int64(0))

		actualResult, actualErr := repo.IsLoginExists(ctx, testCase.login)
		require.NoError(t, actualErr)

		require.Equal(t, testCase.expectedResult, actualResult)

		require.NoError(t, repo.Delete(ctx, testCase.user.ID))
	}
}

func TestUserRepository_GetListByIDs(t *testing.T) {
	ctx := context.Background()

	now := time.Now().UTC()
	joined := time.Date(now.Year(), now.Month(), now.Day(), 12, 0, 0, 0, time.UTC)

	conn := config.MustConnectToDatabase()
	defer conn.Close()

	repo := NewUserRepository(conn)

	testCases := []struct {
		createUsers    []*domain.User
		getUserIndexes []int
	}{
		{
			createUsers: []*domain.User{
				{
					Login:  "test1",
					Email:  "test1@example.org",
					Joined: joined,
				},
				{
					Login:  "test2",
					Email:  "test2@example.org",
					Joined: joined,
				},
				{
					Login:  "test3",
					Email:  "test3@example.org",
					Joined: joined,
				},
				{
					Login:  "test4",
					Email:  "test4@example.org",
					Joined: joined,
				},
				{
					Login:  "test5",
					Email:  "test5@example.org",
					Joined: joined,
				},
				{
					Login:  "test6",
					Email:  "test6@example.org",
					Joined: joined,
				},
				{
					Login:  "test7",
					Email:  "test7@example.org",
					Joined: joined,
				},
				{
					Login:  "test8",
					Email:  "test8@example.org",
					Joined: joined,
				},
				{
					Login:  "test9",
					Email:  "test9@example.org",
					Joined: joined,
				},
				{
					Login:  "test10",
					Email:  "test10@example.org",
					Joined: joined,
				},
			},
			getUserIndexes: []int{0, 4, 9},
		},
	}

	for _, testCase := range testCases {

		for _, user := range testCase.createUsers {
			createErr := repo.Create(ctx, user)
			require.NoError(t, createErr)
			require.Greater(t, user.ID, int64(0))
		}

		userIDs := make([]int64, 0, len(testCase.getUserIndexes))
		expectedUsers := make([]*domain.User, 0, len(testCase.getUserIndexes))
		for _, idx := range testCase.getUserIndexes {
			userIDs = append(userIDs, testCase.createUsers[idx].ID)
			expectedUsers = append(expectedUsers, testCase.createUsers[idx])
		}

		actualUsers, listErr := repo.GetListByIDs(ctx, userIDs)
		require.NoError(t, listErr)

		require.Equal(t, expectedUsers, actualUsers)

		for _, user := range testCase.createUsers {
			require.NoError(t, repo.Delete(ctx, user.ID))
		}
	}
}
