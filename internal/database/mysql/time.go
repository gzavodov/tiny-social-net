package mysql

import (
	"database/sql"
	"fmt"
	"time"
)

const (
	layoutMicroseconds = "2006-01-02 15:04:05.000000"
	layoutTimeSeconds  = "2006-01-02 15:04:05"
	layoutDate         = "2006-01-02"
)

type rawTime []byte

func (t rawTime) DateTime() (time.Time, error) {
	str := string(t)

	switch len(t) {
	case len(layoutDate):
		return time.Parse(layoutDate, str)
	case len(layoutTimeSeconds):
		return time.Parse(layoutTimeSeconds, str)
	case len(layoutMicroseconds):
		return time.Parse(layoutMicroseconds, str)
	default:
		if len(t) > len(layoutTimeSeconds) {
			return time.Parse(layoutTimeSeconds, str[:len(layoutTimeSeconds)])
		}

		if len(t) > len(layoutDate) {
			return time.Parse(layoutDate, str[:len(layoutDate)])
		}

		return time.Date(0001, 1, 1, 00, 00, 00, 00, time.UTC),
			fmt.Errorf("failed to parse time string '%s'", str)
	}
}

func (t rawTime) String() string {
	return string(t)
}

func toNullSqlString(s string) sql.NullString {
	var result sql.NullString
	if len(s) > 0 {
		result.String = s
		result.Valid = true
	}

	return result
}

func toNullSqlDate(t time.Time) sql.NullTime {
	return toNullSqlTime(truncateTime(t))
}

func toNullSqlTime(t time.Time) sql.NullTime {
	var result sql.NullTime
	if !t.IsZero() {
		result.Time = t
		result.Valid = true
	}

	return result
}

func truncateTime(t time.Time) time.Time {
	if t.IsZero() {
		return t
	}

	return t.Truncate(24 * time.Hour)
}
