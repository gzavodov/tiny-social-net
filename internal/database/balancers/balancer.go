package balancers

import (
	"sync"

	"github.com/jmoiron/sqlx"
)

type LoadBalancer struct {
	master   *sqlx.DB
	replicas []*sqlx.DB

	currentReplicaIndex int
	lastReplicaIndex    int
	lock                *sync.Mutex
}

func NewLoadBalancer(master *sqlx.DB, replicas []*sqlx.DB) *LoadBalancer {
	currentReplicaIndex := -1

	lastReplicaIndex := -1
	replicaCount := len(replicas)
	if replicaCount > 0 {
		lastReplicaIndex = replicaCount - 1
	}

	return &LoadBalancer{
		master:              master,
		replicas:            replicas,
		currentReplicaIndex: currentReplicaIndex,
		lastReplicaIndex:    lastReplicaIndex,
		lock:                &sync.Mutex{},
	}
}

func (b *LoadBalancer) GetWriteConnection() *sqlx.DB {
	if b == nil {
		return nil
	}

	return b.master
}

func (b *LoadBalancer) GetReadConnection() *sqlx.DB {
	if b == nil {
		return nil
	}

	return b.getNextReplica()
}

func (b *LoadBalancer) getNextReplica() *sqlx.DB {
	if b == nil {
		return nil
	}

	if b.lastReplicaIndex < 0 {
		return b.master
	}

	if b.lastReplicaIndex < 1 {
		return b.replicas[0]
	}

	b.lock.Lock()
	defer b.lock.Unlock()

	b.currentReplicaIndex++
	if b.currentReplicaIndex > b.lastReplicaIndex {
		b.currentReplicaIndex = 0
	}

	return b.replicas[b.currentReplicaIndex]
}
