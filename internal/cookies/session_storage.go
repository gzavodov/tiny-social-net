package cookies

import (
	"net/http"
	"os"
	"strconv"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"github.com/gorilla/sessions"
)

const (
	requestParamSession = "session"

	sessionKeyUserID    = "id"
	sessionKeyUserLogin = "login"
)

var store = sessions.NewCookieStore([]byte(os.Getenv("TINY_SOCIAL_NET_SESSION_SECRET")))

type UserSessionStorage struct {
}

func (s *UserSessionStorage) GetSession(r *http.Request) security.UserSession {
	session, _ := store.Get(r, requestParamSession)
	return NewUserSession(session)
}

func NewUserSessionStorage() *UserSessionStorage {
	return &UserSessionStorage{}
}

type UserSession struct {
	id      int64
	session *sessions.Session
}

func NewUserSession(session *sessions.Session) *UserSession {
	return (&UserSession{session: session}).initialize()
}

func (s *UserSession) initialize() *UserSession {
	if !s.hasSession() {
		return s
	}

	str := s.getSessionParam(sessionKeyUserID, "")
	if str == "" {
		return s
	}

	id, _ := strconv.ParseInt(str, 10, 64)
	if id > 0 {
		s.id = id
	}

	return s
}

func (s *UserSession) GetID() int64 {
	if s == nil {
		return 0
	}

	return s.id
}

func (s *UserSession) SetID(value int64) {
	if !s.hasSession() {
		return
	}

	s.setSessionParam(sessionKeyUserID, strconv.FormatInt(value, 10))
}

func (s *UserSession) GetLogin() string {
	if s == nil {
		return ""
	}

	return s.getSessionParam(sessionKeyUserLogin, "")
}

func (s *UserSession) SetLogin(value string) {
	if !s.hasSession() {
		return
	}

	s.setSessionParam(sessionKeyUserLogin, value)
}

func (s *UserSession) IsLoggedIn() bool {
	return s.GetID() > 0
}

func (s *UserSession) Reset(r *http.Request, w http.ResponseWriter) error {
	if !s.hasSession() {
		return nil
	}

	s.removeSessionParams(sessionKeyUserID, sessionKeyUserLogin)
	return s.Flush(r, w)
}

func (s *UserSession) hasSession() bool {
	return s != nil && s.session != nil
}

func (s *UserSession) getSessionParam(name string, defaultValue string) string {
	if s == nil || s.session == nil {
		return defaultValue
	}

	v, found := s.session.Values[name]
	if !found || v == nil {
		return defaultValue
	}

	value, success := v.(string)
	if !success {
		return defaultValue
	}

	return value
}

func (s *UserSession) setSessionParam(name string, value string) {
	s.session.Values[name] = value
}

func (s *UserSession) removeSessionParams(names ...string) {
	for _, name := range names {
		delete(s.session.Values, name)
	}
}

func (s *UserSession) Flush(r *http.Request, w http.ResponseWriter) error {
	return s.session.Save(r, w)
}

func (s *UserSession) GetAuthInfo() security.UserAuthInfo {
	return &UserAuthInfo{id: s.GetID(), login: s.GetLogin()}
}

type UserAuthInfo struct {
	id    int64
	login string
}

func (i *UserAuthInfo) GetID() int64 {
	if i == nil {
		return 0
	}

	return i.id
}

func (i *UserAuthInfo) GetLogin() string {
	if i == nil {
		return ""
	}

	return i.login
}

func (i *UserAuthInfo) IsLoggedIn() bool {
	return i != nil && i.id > 0
}
