package security

import (
	"errors"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/dgrijalva/jwt-go"
)

const (
	authorizationHeader = "Authorization"
	jwtClaimsKey        = "jwt_claims"
)

var ErrUnauthorized = errors.New("user is unauthorized")
var ErrInvalidToken = errors.New("security token is not valid")

type (
	JWT struct {
		secret string
	}

	JWTClaims struct {
		jwt.StandardClaims

		ID    int64
		Login string `json:"username"`
		Email string `json:"email"`
	}

	JWTUserAuthInfo struct {
		claims *JWTClaims
	}
)

func NewJWT(secret string) *JWT {
	return &JWT{secret: secret}
}

func (t *JWT) Generate(id int64, login string, email string) (string, error) {
	claims := &JWTClaims{
		ID:    id,
		Login: login,
		Email: email,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().UTC().Add(24 * time.Hour).Unix(),
		},
	}

	return jwt.NewWithClaims(jwt.SigningMethodHS256, claims).
		SignedString([]byte(t.secret))
}

func (t *JWT) Parse(ctx *gin.Context) (*JWTClaims, error) {
	tokenString := ctx.GetHeader(authorizationHeader)
	if tokenString == "" {
		return nil, ErrUnauthorized
	}

	token, err := jwt.ParseWithClaims(
		ctx.GetHeader(authorizationHeader),
		&JWTClaims{},
		func(token *jwt.Token) (interface{}, error) { return []byte(t.secret), nil },
	)

	if err != nil {
		return nil, ErrInvalidToken
	}

	claims, ok := token.Claims.(*JWTClaims)
	if !ok {
		return nil, ErrUnauthorized
	}

	if claims.ExpiresAt < time.Now().UTC().Unix() {
		return nil, ErrUnauthorized
	}

	return claims, nil
}

func (t *JWT) SetJWTClaims(ctx *gin.Context, claims *JWTClaims) {
	ctx.Set(jwtClaimsKey, claims)
}

func (t *JWT) GetJWTClaims(ctx *gin.Context) *JWTClaims {
	value, exists := ctx.Get(jwtClaimsKey)
	if !exists {
		return nil
	}

	return value.(*JWTClaims)
}

func (t *JWT) GetAuthInfo(ctx *gin.Context) UserAuthInfo {
	return NewJWTUserAuthInfo(t.GetJWTClaims(ctx))
}

func NewJWTUserAuthInfo(claims *JWTClaims) *JWTUserAuthInfo {
	return &JWTUserAuthInfo{claims: claims}
}

func (i *JWTUserAuthInfo) GetID() int64 {
	if i == nil || i.claims == nil {
		return 0
	}

	return i.claims.ID
}

func (i *JWTUserAuthInfo) GetLogin() string {
	if i == nil || i.claims == nil {
		return ""
	}

	return i.claims.Login
}

func (i *JWTUserAuthInfo) IsLoggedIn() bool {
	if i == nil || i.claims == nil {
		return false
	}

	return i.claims.ID > 0
}
