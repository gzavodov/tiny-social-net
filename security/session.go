package security

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const sessionKey = "session"

type (
	UserSession interface {
		IsLoggedIn() bool

		GetID() int64
		SetID(value int64)

		GetLogin() string
		SetLogin(value string)

		GetAuthInfo() UserAuthInfo

		Reset(r *http.Request, w http.ResponseWriter) error
		Flush(r *http.Request, w http.ResponseWriter) error
	}

	UserSessionStorage interface {
		GetSession(r *http.Request) UserSession
	}
)

func SetSession(c *gin.Context, sess UserSession) {
	c.Set(sessionKey, sess)
}

func GetSession(c *gin.Context) UserSession {
	value, exists := c.Get(sessionKey)
	if !exists {
		return nil
	}

	return value.(UserSession)
}

func GetAuthInfo(c *gin.Context) UserAuthInfo {
	return GetSession(c).GetAuthInfo()
}
