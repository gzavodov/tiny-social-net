package security

import "github.com/gin-gonic/gin"

type (
	UserAuthInfo interface {
		GetID() int64
		GetLogin() string

		IsLoggedIn() bool
	}

	UserAuthInfoProvider interface {
		GetAuthInfo(*gin.Context) UserAuthInfo
	}
)
