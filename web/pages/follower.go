package pages

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	FollowerController struct {
		PageController

		followerRepo domain.FollowerRepository
		userRepo     domain.UserRepository
	}
)

func NewFollowerController(followerRepo domain.FollowerRepository, userRepo domain.UserRepository) *FollowerController {
	return &FollowerController{followerRepo: followerRepo, userRepo: userRepo}
}

// Followers route
func (f *FollowerController) Followers(c *gin.Context) {
	userIDParam := c.Param("id")
	if userIDParam == "" {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("parameter 'id' must be specified"))
		return
	}

	userID, err := strconv.ParseInt(userIDParam, 10, 64)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	user, err := f.userRepo.Get(c, userID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	limit := 0
	offset := 0

	limitParam := c.Query("limit")
	if limitParam != "" {
		n, err := strconv.ParseInt(limitParam, 10, 32)
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		limit = int(n)

	}

	if limit <= 0 {
		limit = 50
	}

	offsetParam := c.Query("offset")
	if limitParam != "" {
		n, err := strconv.ParseInt(offsetParam, 10, 32)
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		offset = int(n)

	}

	followerIDs, err := f.followerRepo.GetFollowByList(c, userID, limit, offset)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followToCount, err := f.followerRepo.GetFollowToCount(c, followerIDs)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followerUserList, err := f.userRepo.GetListByIDs(c, followerIDs)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followers := make([]interface{}, 0, len(followerUserList))
	for _, follower := range followerUserList {
		followers = append(
			followers,
			map[string]interface{}{
				"id":                follower.ID,
				"username":          follower.Login,
				"email":             follower.Email,
				"numberOfFollowers": followToCount[follower.ID],
			},
		)
	}

	f.renderHTMLTemplate(
		c,
		"followers",
		gin.H{
			"title":     fmt.Sprintf("%s's Followers", user.Login),
			"session":   f.prepareSessionData(c),
			"followers": followers,
			"no_mssg":   fmt.Sprintf("%s have no followers!!", user.Login),
		},
	)
}

// Followings route
func (f *FollowerController) Followings(c *gin.Context) {
	userIDParam := c.Param("id")
	if userIDParam == "" {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("parameter 'id' must be specified"))
		return
	}

	userID, err := strconv.ParseInt(userIDParam, 10, 64)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	user, err := f.userRepo.Get(c, userID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	limit := 0
	offset := 0

	limitParam := c.Query("limit")
	if limitParam != "" {
		n, err := strconv.ParseInt(limitParam, 10, 32)
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		limit = int(n)

	}

	if limit <= 0 {
		limit = 50
	}

	offsetParam := c.Query("offset")
	if limitParam != "" {
		n, err := strconv.ParseInt(offsetParam, 10, 32)
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		offset = int(n)

	}

	followingsIDs, err := f.followerRepo.GetFollowToList(c, userID, limit, offset)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followToCount, err := f.followerRepo.GetFollowToCount(c, followingsIDs)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followingUserList, err := f.userRepo.GetListByIDs(c, followingsIDs)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followings := make([]interface{}, 0, len(followingUserList))
	for _, following := range followingUserList {
		followings = append(
			followings,
			map[string]interface{}{
				"id":                following.ID,
				"username":          following.Login,
				"email":             following.Email,
				"numberOfFollowers": followToCount[following.ID],
			},
		)
	}

	f.renderHTMLTemplate(
		c,
		"followings",
		gin.H{
			"title":      fmt.Sprintf("%s's Followings", user.Login),
			"session":    f.prepareSessionData(c),
			"followings": followings,
			"no_mssg":    fmt.Sprintf("%s have no followings!!", user.Login),
		},
	)
}
