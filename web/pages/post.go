package pages

import (
	"errors"
	"net/http"
	"strconv"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/gin-gonic/gin"
)

type (
	PostController struct {
		PageController

		postRepo domain.PostRepository
		likeRepo domain.LikeRepository
	}
)

func NewPostController(postRepo domain.PostRepository, likeRepo domain.LikeRepository) *PostController {
	return &PostController{postRepo: postRepo, likeRepo: likeRepo}
}

// CreatePost route
func (p *PostController) CreatePost(c *gin.Context) {
	p.renderHTMLTemplate(
		c,
		"create_post",
		gin.H{
			"title":   "Create Post",
			"session": p.prepareSessionData(c),
		},
	)
}

// ViewPost route
func (p *PostController) ViewPost(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("parameter 'id' must be specified"))
		return
	}

	postID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	post, err := p.postRepo.Get(c, postID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if post == nil {
		c.Redirect(http.StatusFound, "/404")
		return
	}

	isLiked, err := p.likeRepo.IsLiked(c, post.ID, security.GetAuthInfo(c).GetID())
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	likeCount, err := p.likeRepo.GetCount(c, post.ID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	p.renderHTMLTemplate(
		c,
		"view_post",
		gin.H{
			"title":   "View Post",
			"session": p.prepareSessionData(c),
			"post": gin.H{
				"postID":    post.ID,
				"title":     post.Title,
				"content":   post.Content,
				"createdBy": post.CreatedBy,
				"createdAt": post.CreatedAt,
			},
			"postCreatedBy": strconv.FormatInt(post.CreatedBy, 10),
			"isLiked":       isLiked,
			"likes":         likeCount,
		},
	)
}

// EditPost route
func (p *PostController) EditPost(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("parameter 'id' must be specified"))
		return
	}

	postID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	post, err := p.postRepo.Get(c, postID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	if post == nil {
		c.Redirect(http.StatusFound, "/404")
		return
	}

	if post.CreatedBy != security.GetAuthInfo(c).GetID() {
		c.Redirect(http.StatusFound, "/404")
		return
	}

	p.renderHTMLTemplate(
		c,
		"edit_post",
		gin.H{
			"title":   "Edit Post",
			"session": p.prepareSessionData(c),
			"post": gin.H{
				"postID":  post.ID,
				"title":   post.Title,
				"content": post.Content,
			},
		},
	)
}
