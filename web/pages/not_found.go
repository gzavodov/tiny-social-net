package pages

import (
	"github.com/gin-gonic/gin"
)

type (
	NotFoundController struct {
		PageController
	}
)

func NewNotFoundController() *NotFoundController {
	return &NotFoundController{}
}

// Default route
func (p *NotFoundController) Default(c *gin.Context) {
	p.renderHTMLTemplate(
		c,
		"404",
		gin.H{
			"title":   "Page is not found",
			"session": p.prepareSessionData(c),
		},
	)
}
