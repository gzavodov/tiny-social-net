package pages

import (
	"errors"
	"net/http"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/gin-gonic/gin"
)

type IndexController struct {
	PageController

	postRepo domain.PostRepository
	userRepo domain.UserRepository
}

func NewIndexController(postRepo domain.PostRepository, userRepo domain.UserRepository) *IndexController {
	return &IndexController{postRepo: postRepo, userRepo: userRepo}
}

// Default route
func (i *IndexController) Default(c *gin.Context) {
	posts, err := i.postRepo.GetFollowingList(c, security.GetAuthInfo(c).GetID(), 0, 0)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("failed to get user post list"))
		return
	}

	userIDs := make([]int64, 0, len(posts))
	for _, post := range posts {
		userIDs = append(userIDs, post.CreatedBy)
	}

	userLogins, err := i.userRepo.GetLoginByIDs(c, userIDs)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("failed to get user logins"))
		return
	}

	feeds := make([]interface{}, 0, len(posts))
	for _, post := range posts {
		feeds = append(feeds,
			map[string]interface{}{
				"postID":            post.ID,
				"title":             post.Title,
				"content":           post.Content,
				"createdBy":         post.CreatedBy,
				"createdByUsername": userLogins[post.CreatedBy],
				"createdAt":         post.CreatedAt,
			},
		)
	}

	i.renderHTMLTemplate(c, "index", gin.H{
		"title":   "Home",
		"session": i.prepareSessionData(c),
		"posts":   feeds,
	})
}
