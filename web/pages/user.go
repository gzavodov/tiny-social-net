package pages

import (
	"fmt"
	"net/http"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/gin-gonic/gin"
)

type (
	UserController struct {
		PageController

		userRepo       domain.UserRepository
		sessionStorage security.UserSessionStorage
	}
)

func NewUserController(userRepo domain.UserRepository, sessionStorage security.UserSessionStorage) *UserController {
	return &UserController{userRepo: userRepo, sessionStorage: sessionStorage}
}

// Signup route
func (u *UserController) Signup(c *gin.Context) {
	u.renderHTMLTemplate(
		c,
		"signup",
		gin.H{"title": "Signup For Free"},
	)
}

// Login route
func (u *UserController) Login(c *gin.Context) {
	u.renderHTMLTemplate(
		c,
		"login",
		gin.H{"title": "Login To Continue"},
	)
}

// Logout route
func (u *UserController) Logout(c *gin.Context) {
	sess := u.sessionStorage.GetSession(c.Request)
	if err := sess.Reset(c.Request, c.Writer); err != nil {
		_ = c.AbortWithError(
			http.StatusInternalServerError,
			fmt.Errorf("failed to reset user session: %w", err),
		)
	}

	c.Redirect(http.StatusFound, "/login")
}

// Explore route
func (u *UserController) Explore(c *gin.Context) {
	list, err := u.userRepo.GetList(c, nil, 10, 0)
	if err != nil {
		_ = c.AbortWithError(
			http.StatusInternalServerError,
			fmt.Errorf("failed to get user list: %w", err),
		)
	}

	data := make([]interface{}, 0, len(list))
	for _, item := range list {
		data = append(data,
			map[string]interface{}{
				"id":              item.ID,
				"username":        item.Login,
				"email":           item.Email,
				"followers_count": 0,
			},
		)
	}

	u.renderHTMLTemplate(
		c,
		"explore",
		gin.H{
			"title":   "Explore",
			"session": u.prepareSessionData(c),
			"users":   data,
		},
	)
}
