package pages

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"github.com/gin-gonic/gin"
)

type PageController struct {
}

func (p PageController) renderHTMLTemplate(c *gin.Context, name string, data interface{}) {
	c.HTML(http.StatusOK, fmt.Sprintf("%s.html", name), data)
}

func (p PageController) prepareSessionData(c *gin.Context) interface{} {
	authInfo := security.GetAuthInfo(c)
	if authInfo == nil {
		return nil
	}

	return map[string]interface{}{
		"id":       strconv.FormatInt(authInfo.GetID(), 10),
		"username": authInfo.GetLogin(),
	}
}
