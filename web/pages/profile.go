package pages

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	ProfileController struct {
		PageController

		userRepo        domain.UserRepository
		profileViewRepo domain.ProfileViewRepository
		followerRepo    domain.FollowerRepository
		postRepo        domain.PostRepository
	}
)

func NewProfileController(
	userRepo domain.UserRepository,
	profileViewRepo domain.ProfileViewRepository,
	followerRepo domain.FollowerRepository,
	postRepo domain.PostRepository,
) *ProfileController {
	return &ProfileController{
		userRepo:        userRepo,
		profileViewRepo: profileViewRepo,
		followerRepo:    followerRepo,
		postRepo:        postRepo,
	}
}

// View route
func (p *ProfileController) View(c *gin.Context) {
	userIDParam := c.Param("id")
	if userIDParam == "" {
		_ = c.AbortWithError(http.StatusBadRequest, errors.New("parameter 'id' must be specified"))
		return
	}

	userID, err := strconv.ParseInt(userIDParam, 10, 64)
	if err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	if err := p.profileViewRepo.Create(c, security.GetAuthInfo(c).GetID(), userID, time.Now().UTC()); err != nil {
		_ = c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	viewCount, err := p.profileViewRepo.GetCount(c, []int64{userID})
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followByCount, err := p.followerRepo.GetFollowByCount(c, []int64{userID})
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	followToCount, err := p.followerRepo.GetFollowToCount(c, []int64{userID})
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	isFollowed, err := p.followerRepo.IsExists(c, security.GetAuthInfo(c).GetID(), userID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	user, err := p.userRepo.Get(c, userID)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	postList, err := p.postRepo.GetList(c, userID, 0, 0)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	posts := make([]interface{}, 0, len(postList))
	for _, post := range postList {
		posts = append(posts,
			map[string]interface{}{
				"postID":            post.ID,
				"title":             post.Title,
				"content":           post.Content,
				"createdBy":         user.ID,
				"createdByUsername": user.Login,
				"createdAt":         post.CreatedAt,
			},
		)
	}

	p.renderHTMLTemplate(
		c,
		"profile",
		gin.H{
			"title":   fmt.Sprintf("@%s", user.Login),
			"session": p.prepareSessionData(c),
			"user": gin.H{
				"id":       strconv.FormatInt(user.ID, 10),
				"username": user.Login,
				"email":    user.Email,
				"bio":      user.Bio,
			},
			"posts":       posts,
			"followers":   followByCount[userID],
			"followings":  followToCount[userID],
			"is_followed": isFollowed,
			"views":       viewCount[userID],
			"no_mssg":     fmt.Sprintf("%s has no posts!!", user.Login),
		},
	)
}

// Edit route
func (p *ProfileController) Edit(c *gin.Context) {
	user, err := p.userRepo.Get(c, security.GetAuthInfo(c).GetID())
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	p.renderHTMLTemplate(
		c,
		"edit_profile",
		gin.H{
			"title":    "Edit Profile",
			"session":  p.prepareSessionData(c),
			"id":       strconv.FormatInt(user.ID, 10),
			"username": user.Login,
			"email":    user.Email,
			"bio":      user.Bio,
			"joined":   user.Joined,
		},
	)
}
