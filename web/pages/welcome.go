package pages

import (
	"github.com/gin-gonic/gin"
)

type (
	WelcomeController struct {
		PageController
	}
)

func NewWelcomeController() *WelcomeController {
	return &WelcomeController{}
}

// Default route
func (pc *WelcomeController) Default(c *gin.Context) {
	pc.renderHTMLTemplate(
		c,
		"welcome",
		gin.H{
			"title": "Welcome!",
		},
	)
}
