package actions

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/web"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/gin-gonic/gin"
)

type (
	UserController struct {
		userRepo       domain.UserRepository
		sessionStorage security.UserSessionStorage
	}

	UserSignupRequest struct {
		Login         string
		Email         string
		Password      string
		PasswordAgain string
		c             *gin.Context
		userRepo      domain.UserRepository
	}

	UserLoginRequest struct {
		Login    string
		Password string
		c        *gin.Context
	}
)

func NewUserController(userRepo domain.UserRepository, sessionStorage security.UserSessionStorage) *UserController {
	return &UserController{userRepo: userRepo, sessionStorage: sessionStorage}
}

// UserSignup function to register user
func (u *UserController) UserSignup(c *gin.Context) {
	request := NewSignupRequest(c, u.userRepo)
	response := web.NewResponse(c)

	if err := request.Validate(); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	passwordHash, err := domain.GeneratePasswordHash(request.Password)
	if err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	user := &domain.User{
		Login:    request.Login,
		Email:    request.Email,
		Password: passwordHash,
		Joined:   time.Now().UTC(),
	}

	if err := u.userRepo.Create(c, user); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	sess := u.sessionStorage.GetSession(c.Request)
	sess.SetID(user.ID)
	sess.SetLogin(user.Login)
	if err := sess.Flush(c.Request, c.Writer); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	response.
		AddMessage(fmt.Sprintf("Hello, %s!", user.Login)).
		WriteJSON()
}

// UserLogin function to log user in
func (u *UserController) UserLogin(c *gin.Context) {
	request := NewLoginRequest(c)
	response := web.NewResponse(c)

	if err := request.Validate(); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	user, err := u.userRepo.GetByLogin(c, request.Login)
	if err != nil {
		response.Abort(http.StatusInternalServerError)
		return
	}

	if user == nil || !domain.CheckPasswordHash(request.Password, user.Password) {
		response.AddError(errors.New("user login or password is invalid")).WriteJSON()
		return
	}

	sess := u.sessionStorage.GetSession(c.Request)
	sess.SetID(user.ID)
	sess.SetLogin(user.Login)
	if err := sess.Flush(c.Request, c.Writer); err != nil {
		response.Abort(http.StatusInternalServerError)
		return
	}

	response.
		AddMessage(fmt.Sprintf("Hello, %s!", user.Login)).
		WriteJSON()
}

func NewLoginRequest(c *gin.Context) *UserLoginRequest {
	return &UserLoginRequest{
		Login:    strings.TrimSpace(c.PostForm("username")),
		Password: strings.TrimSpace(c.PostForm("password")),
		c:        c,
	}
}

func (r *UserLoginRequest) Validate() error {
	if len(r.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(r.Password) == 0 {
		return errors.New("password is not specified")
	}

	return nil
}

func NewSignupRequest(c *gin.Context, userRepo domain.UserRepository) *UserSignupRequest {
	return &UserSignupRequest{
		Login:         strings.TrimSpace(c.PostForm("username")),
		Email:         strings.TrimSpace(c.PostForm("email")),
		Password:      strings.TrimSpace(c.PostForm("password")),
		PasswordAgain: strings.TrimSpace(c.PostForm("password_again")),
		c:             c,
		userRepo:      userRepo,
	}
}

func (r *UserSignupRequest) Validate() error {
	if len(r.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(r.Login) < 4 || len(r.Login) > 32 {
		return errors.New("login should be between 4 and 32")
	}

	if len(r.Password) == 0 {
		return errors.New("password is not specified")
	}

	if len(r.PasswordAgain) == 0 {
		return errors.New("password_again is not specified")
	}

	if r.Password != r.PasswordAgain {
		return errors.New("passwords don't match")
	}

	//mailErr := checkmail.ValidateFormat(email)
	if len(r.Email) == 0 {
		return errors.New("email is not specified")
	}
	isLoginExists, err := r.userRepo.IsLoginExists(r.c, r.Login)
	if err != nil {
		return err
	}

	if isLoginExists {
		return errors.New("login already exists")
	}

	isEmailAddressExists, err := r.userRepo.IsEmailAddressExists(r.c, r.Email)
	if err != nil {
		return err
	}

	if isEmailAddressExists {
		return errors.New("email already exists")
	}

	return nil
}
