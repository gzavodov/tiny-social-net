package actions

import (
	"errors"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/web"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	LikeController struct {
		likeRepo domain.LikeRepository
	}

	LikeRequest struct {
		Request

		PostID int64
	}
)

func NewLikeController(likeRepo domain.LikeRepository) *LikeController {
	return &LikeController{likeRepo: likeRepo}
}

// Like route
func (f *LikeController) Like(c *gin.Context) {
	request := NewLikeRequest(c)
	response := web.NewResponse(c)
	defer response.WriteJSON()

	if err := request.Parse(); err != nil {
		response.AddError(err)
		return
	}

	if err := request.Validate(); err != nil {
		response.AddError(err)
		return
	}

	isLiked, err := f.likeRepo.IsLiked(c, request.PostID, security.GetAuthInfo(c).GetID())
	if err != nil {
		response.AddError(err)
		return
	}

	if isLiked {
		response.AddMessage("Post already Liked!!")
		return
	}

	if err := f.likeRepo.Like(c, request.PostID, security.GetAuthInfo(c).GetID(), time.Now().UTC()); err != nil {
		response.AddError(err)
		return
	}

	response.AddMessage("Post Liked!!")
}

// Unlike route
func (f *LikeController) Unlike(c *gin.Context) {
	request := NewLikeRequest(c)
	response := web.NewResponse(c)
	defer response.WriteJSON()

	if err := request.Parse(); err != nil {
		response.AddError(err)
		return
	}

	if err := request.Validate(); err != nil {
		response.AddError(err)
		return
	}

	if err := f.likeRepo.UnLike(c, request.PostID, security.GetAuthInfo(c).GetID()); err != nil {
		response.AddError(err)
		return
	}

	response.AddMessage("Post Unliked!!")
}

func NewLikeRequest(c *gin.Context) *LikeRequest {
	return &LikeRequest{Request: Request{c: c}}
}

func (r *LikeRequest) Parse() error {
	postID, err := r.ParseParamInt64("post")
	if err != nil {
		return errors.New("could not parse post id")
	}

	r.PostID = postID
	return nil
}

func (r *LikeRequest) Validate() error {
	if r.PostID <= 0 {
		return errors.New("post id must be greater than zero")
	}

	return nil
}
