package actions

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/web"

	"github.com/badoux/checkmail"
	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	ProfileController struct {
		userRepo domain.UserRepository
	}

	ProfileUpdateRequest struct {
		Login string
		Email string
		Bio   string
		c     *gin.Context
	}
)

func NewProfileController(userRepo domain.UserRepository) *ProfileController {
	return &ProfileController{userRepo: userRepo}
}

// Update update user profile
func (p *ProfileController) Update(c *gin.Context) {
	request := NewProfileUpdateRequest(c)
	response := web.NewResponse(c)
	defer response.WriteJSON()

	if err := request.Validate(); err != nil {
		response.AddError(err)
		return
	}

	user, err := p.userRepo.Get(c, security.GetAuthInfo(c).GetID())
	if err != nil {
		response.AddError(fmt.Errorf("failed to get user: %w", err))
		return
	}

	isChanged := false
	if user.Login != request.Login {
		user.Login = request.Login
		isChanged = true
	}

	if user.Email != request.Email {
		user.Email = request.Email
		isChanged = true
	}

	if user.Bio != request.Bio {
		user.Bio = request.Bio
		isChanged = true
	}

	if !isChanged {
		response.AddMessage("Profile is not changed!!")
		return
	}

	if err := p.userRepo.Update(c, user); err != nil {
		response.AddError(err)
		return
	}

	response.
		AddMessage("Profile is updated!!")
}

// ChangeAvatar route
func (p *ProfileController) ChangeAvatar(c *gin.Context) {
	response := web.NewResponse(c)
	defer response.WriteJSON()

	baseDir, _ := os.Getwd()
	avatarDir := fmt.Sprintf("%s/public/users/%d", baseDir, security.GetAuthInfo(c).GetID())
	if _, err := os.Stat(avatarDir); os.IsNotExist(err) {
		err := os.Mkdir(avatarDir, 0744)
		if err != nil {
			response.AddError(fmt.Errorf("failed to create directory: %w", err))
			return
		}
	}

	avatarPath := fmt.Sprintf("%s/avatar.png", avatarDir)
	if err := os.Remove(avatarPath); err != nil {
		if !os.IsNotExist(err) {
			response.AddError(fmt.Errorf("failed to remove file: %w", err))
			return
		}
	}

	file, _ := c.FormFile("avatar")
	if err := c.SaveUploadedFile(file, avatarPath); err != nil {
		response.AddError(fmt.Errorf("failed to save file: %w", err))
		return
	}

	response.AddMessage("Avatar changed!!")
}

func NewProfileUpdateRequest(c *gin.Context) *ProfileUpdateRequest {
	return &ProfileUpdateRequest{
		Login: strings.TrimSpace(c.PostForm("username")),
		Email: strings.TrimSpace(c.PostForm("email")),
		Bio:   strings.TrimSpace(c.PostForm("bio")),
		c:     c,
	}
}

func (r *ProfileUpdateRequest) Validate() error {
	if len(r.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(r.Email) == 0 {
		return errors.New("email is not specified")
	}

	if err := checkmail.ValidateFormat(r.Email); err != nil {
		return fmt.Errorf("email: %w", err)
	}

	return nil
}
