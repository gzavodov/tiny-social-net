package actions

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/web"
)

type (
	FollowerController struct {
		followerRepo domain.FollowerRepository
		userRepo     domain.UserRepository
	}

	FollowRequest struct {
		Request

		FollowTo int64
	}
)

func NewFollowerController(followerRepo domain.FollowerRepository, userRepo domain.UserRepository) *FollowerController {
	return &FollowerController{followerRepo: followerRepo, userRepo: userRepo}
}

// Follow route
func (f *FollowerController) Follow(c *gin.Context) {
	request := NewFollowRequest(c)
	response := web.NewResponse(c)
	defer response.WriteJSON()

	if err := request.Parse(); err != nil {
		response.AddError(err)
		return
	}

	if err := request.Validate(); err != nil {
		response.AddError(err)
		return
	}

	followToUser, err := f.userRepo.Get(c, request.FollowTo)
	if err != nil {
		response.AddError(fmt.Errorf("failed to get follow to user: %w", err))
		return
	}

	if err := f.followerRepo.Create(c, security.GetAuthInfo(c).GetID(), request.FollowTo, time.Now().UTC()); err != nil {
		response.AddError(err)
		return
	}

	response.AddMessage(fmt.Sprintf("Followed to %s!", followToUser.Login))
}

// Unfollow route
func (f *FollowerController) Unfollow(c *gin.Context) {
	request := NewFollowRequest(c)
	response := web.NewResponse(c)
	defer response.WriteJSON()

	if err := request.Parse(); err != nil {
		response.AddError(err)
		return
	}

	if err := request.Validate(); err != nil {
		response.AddError(err)
		return
	}

	followToUser, err := f.userRepo.Get(c, request.FollowTo)
	if err != nil {
		response.AddError(fmt.Errorf("failed to get follow to user: %w", err))
		return
	}

	if err := f.followerRepo.Delete(c, security.GetAuthInfo(c).GetID(), request.FollowTo); err != nil {
		response.AddError(err)
		return
	}

	response.AddMessage(fmt.Sprintf("Unfollowed to %s!", followToUser.Login))
}

func NewFollowRequest(c *gin.Context) *FollowRequest {
	return &FollowRequest{Request: Request{c: c}}
}

func (r *FollowRequest) Parse() error {
	userID, err := r.ParseParamInt64("user")
	if err != nil {
		return errors.New("could not parse user id")
	}

	r.FollowTo = userID
	return nil
}

func (r *FollowRequest) Validate() error {
	if r.FollowTo <= 0 {
		return errors.New("user id must be greater than zero")
	}

	return nil
}
