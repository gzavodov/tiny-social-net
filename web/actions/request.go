package actions

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

type Request struct {
	c *gin.Context
}

func (r *Request) ParseParamInt64(name string) (int64, error) {
	str := strings.TrimSpace(r.c.PostForm(name))
	if len(str) == 0 {
		return 0, errors.New("parameter is not found")
	}

	num, err := strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0, fmt.Errorf("failed to parse string as integer: %s", str)
	}

	return num, nil
}
