package actions

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/caches"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/web"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	PostController struct {
		postRepo                 domain.PostRepository
		followerRepo             domain.FollowerRepository
		userFeedCacheInvalidator caches.UserFeedCacheInvalidator
	}

	CreatePostRequest struct {
		Title   string
		Content string
		c       *gin.Context
	}

	UpdatePostRequest struct {
		PostID  string
		Title   string
		Content string
		c       *gin.Context
	}

	DeletePostRequest struct {
		PostID string
		c      *gin.Context
	}
)

func NewPostController(
	postRepo domain.PostRepository,
	followerRepo domain.FollowerRepository,
	userFeedCacheInvalidator caches.UserFeedCacheInvalidator,
) *PostController {
	return &PostController{
		postRepo:                 postRepo,
		followerRepo:             followerRepo,
		userFeedCacheInvalidator: userFeedCacheInvalidator,
	}
}

// CreatePost route
func (pc *PostController) CreatePost(c *gin.Context) {
	request := NewCreatePostRequest(c)
	response := web.NewResponse(c)

	if err := request.Validate(); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	userID := security.GetAuthInfo(c).GetID()

	post := &domain.Post{
		Title:     request.Title,
		Content:   request.Content,
		CreatedBy: userID,
		CreatedAt: time.Now().UTC(),
	}

	if err := pc.postRepo.Create(c, post); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	if err := pc.invalidateUserCaches(c, userID); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	response.
		AddField("postID", post.ID).
		AddMessage("Post is created").
		WriteJSON()
}

// UpdatePost route
func (pc *PostController) UpdatePost(c *gin.Context) {
	request := NewUpdatePostRequest(c)
	response := web.NewResponse(c)

	if err := request.Validate(); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	postID, err := request.ParsePostID()
	if err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	post, err := pc.postRepo.Get(c, postID)
	if err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	if post == nil {
		response.AddError(errors.New("post is not found")).WriteJSON()
		return
	}

	userID := security.GetAuthInfo(c).GetID()

	if post.CreatedBy != userID {
		response.AddError(errors.New("post is not found")).WriteJSON()
		return
	}

	if post.Title == request.Title && post.Content == request.Content {
		response.
			AddMessage("Post is not changed").
			WriteJSON()
		return
	}

	post.Title = request.Title
	post.Content = request.Content

	if err := pc.postRepo.Update(c, post); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	if err := pc.invalidateUserCaches(c, userID); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	response.
		AddMessage("Post is updated").
		WriteJSON()
}

// DeletePost route
func (pc *PostController) DeletePost(c *gin.Context) {
	request := NewDeletePostRequest(c)
	response := web.NewResponse(c)

	if err := request.Validate(); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	postID, err := request.ParsePostID()
	if err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	post, err := pc.postRepo.Get(c, postID)
	if err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	if post == nil {
		response.AddError(errors.New("post is not found")).WriteJSON()
		return
	}

	userID := security.GetAuthInfo(c).GetID()

	if post.CreatedBy != userID {
		response.AddError(errors.New("post is not found")).WriteJSON()
		return
	}

	if err := pc.postRepo.Delete(c, post.ID); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	if err := pc.invalidateUserCaches(c, userID); err != nil {
		response.AddError(err).WriteJSON()
		return
	}

	response.
		AddMessage("Post is deleted").
		WriteJSON()
}

func (pc *PostController) invalidateUserCaches(ctx context.Context, userID int64) error {
	followerIDs, err := pc.followerRepo.GetFollowByList(ctx, userID, 0, 0)
	if err != nil {
		return err
	}

	pc.userFeedCacheInvalidator.InvalidateEntries(followerIDs)
	return nil
}

func NewCreatePostRequest(c *gin.Context) *CreatePostRequest {
	return &CreatePostRequest{
		Title:   strings.TrimSpace(c.PostForm("title")),
		Content: strings.TrimSpace(c.PostForm("content")),
		c:       c,
	}
}

func (r *CreatePostRequest) Validate() error {
	if len(r.Title) == 0 {
		return errors.New("title is not specified")
	}

	if len(r.Content) == 0 {
		return errors.New("content is not specified")
	}

	return nil
}

func NewUpdatePostRequest(c *gin.Context) *UpdatePostRequest {
	return &UpdatePostRequest{
		PostID:  strings.TrimSpace(c.PostForm("postID")),
		Title:   strings.TrimSpace(c.PostForm("title")),
		Content: strings.TrimSpace(c.PostForm("content")),
		c:       c,
	}
}

func (r *UpdatePostRequest) ParsePostID() (int64, error) {
	return strconv.ParseInt(r.PostID, 10, 64)
}

func (r *UpdatePostRequest) Validate() error {
	if len(r.PostID) == 0 {
		return errors.New("postID is not specified")
	}

	if _, err := r.ParsePostID(); err != nil {
		return fmt.Errorf("invalid postID is specified: %w", err)
	}

	if len(r.Title) == 0 {
		return errors.New("title is not specified")
	}

	if len(r.Content) == 0 {
		return errors.New("content is not specified")
	}

	return nil
}

func NewDeletePostRequest(c *gin.Context) *DeletePostRequest {
	return &DeletePostRequest{
		PostID: strings.TrimSpace(c.PostForm("post")),
		c:      c,
	}
}

func (r *DeletePostRequest) ParsePostID() (int64, error) {
	return strconv.ParseInt(r.PostID, 10, 64)
}

func (r *DeletePostRequest) Validate() error {
	if len(r.PostID) == 0 {
		return errors.New("postID is not specified")
	}

	if _, err := r.ParsePostID(); err != nil {
		return fmt.Errorf("invalid postID is specified: %w", err)
	}

	return nil
}
