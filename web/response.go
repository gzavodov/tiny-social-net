package web

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type Response struct {
	c           *gin.Context
	fields      map[string]interface{}
	messageList []string
	errorList   []error
}

func NewResponse(c *gin.Context) *Response {
	return &Response{
		c:      c,
		fields: make(map[string]interface{}),
	}
}

func (r *Response) AddField(name string, value interface{}) *Response {
	r.fields[name] = value
	return r
}

func (r *Response) AddMessage(msg string) *Response {
	r.messageList = append(r.messageList, msg)
	return r
}

func (r *Response) AddError(err error) *Response {
	if err == nil {
		return r
	}

	r.errorList = append(r.errorList, err)
	return r
}

func (r *Response) WriteJSON() *Response {
	r.c.JSON(http.StatusOK, r.prepareData())
	return r
}

func (r *Response) Abort(code int) *Response {
	r.c.AbortWithStatus(code)
	return r
}

func (r *Response) prepareData() map[string]interface{} {
	data := make(map[string]interface{})
	for k, v := range r.fields {
		data[k] = v
	}

	data["success"] = true

	messages := make([]string, 0, len(r.messageList)+len(r.errorList))
	if len(r.messageList) > 0 {
		messages = append(messages, r.messageList...)
	}

	if len(r.errorList) > 0 {
		data["success"] = false
		for _, err := range r.errorList {
			messages = append(messages, fmt.Sprintf("ERROR: %s", err.Error()))
		}
	}

	data["mssg"] = strings.Join(messages, "\n")

	return data
}
