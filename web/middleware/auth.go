package middleware

import (
	"net/http"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"github.com/gin-gonic/gin"
)

type AuthenticationController struct {
	sessionStorage security.UserSessionStorage
}

func NewAuthenticationController(sessionStorage security.UserSessionStorage) *AuthenticationController {
	return &AuthenticationController{sessionStorage: sessionStorage}
}

func (a *AuthenticationController) EnsureLoggedIn(c *gin.Context) {
	sess := a.sessionStorage.GetSession(c.Request)
	if !sess.IsLoggedIn() {
		c.Redirect(http.StatusFound, "/login")
		return
	}

	security.SetSession(c, sess)
	c.Next()
}

func (a *AuthenticationController) EnsureNotLoggedIn(c *gin.Context) {
	sess := a.sessionStorage.GetSession(c.Request)
	if sess.IsLoggedIn() {
		c.Redirect(http.StatusFound, "/")
		return
	}

	c.Next()
}
