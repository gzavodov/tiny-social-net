package posts

import (
	"context"
	"errors"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/caches"

	"gitlab.com/gzavodov/tiny-social-net/api/requests"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/api/responses"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	postRepo                 domain.PostRepository
	followerRepo             domain.FollowerRepository
	userAuthInfoProvider     security.UserAuthInfoProvider
	userFeedCacheInvalidator caches.UserFeedCacheInvalidator
}

func NewHandler(
	postRepo domain.PostRepository,
	followerRepo domain.FollowerRepository,
	userAuthInfoProvider security.UserAuthInfoProvider,
	userFeedCacheInvalidator caches.UserFeedCacheInvalidator,
) *Handler {
	return &Handler{
		postRepo:                 postRepo,
		followerRepo:             followerRepo,
		userAuthInfoProvider:     userAuthInfoProvider,
		userFeedCacheInvalidator: userFeedCacheInvalidator,
	}
}

// Create route
func (h *Handler) Create(ctx *gin.Context) {
	request := NewCreatePostRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateCreatePostRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	post := &domain.Post{
		Title:     request.Title,
		Content:   request.Content,
		CreatedBy: userID,
		CreatedAt: time.Now().UTC(),
	}

	if err := h.postRepo.Create(ctx, post); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if err := h.invalidateUserCaches(ctx, userID); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(gin.H{"id": strconv.FormatInt(post.ID, 10)}).
		WriteJSON(ctx)
}

// Get route
func (h *Handler) Get(ctx *gin.Context) {
	response := responses.New()

	postID, err := requests.GetIntegerURLParam(ctx, "id")
	if err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	post, err := h.postRepo.Get(ctx, postID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if post == nil {
		response.Abort(ctx, http.StatusNotFound, errors.New("post is not found"))
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(
			gin.H{
				"id":        post.ID,
				"title":     post.Title,
				"content":   post.Content,
				"createdBy": post.CreatedBy,
				"createdAt": post.CreatedAt,
			},
		).
		WriteJSON(ctx)
}

// Update route
func (h *Handler) Update(ctx *gin.Context) {
	request := NewUpdatePostRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateUpdatePostRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	post, err := h.postRepo.Get(ctx, request.ID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if post == nil {
		response.Abort(ctx, http.StatusNotFound, errors.New("post is not found"))
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	if post.CreatedBy != userID {
		response.Abort(ctx, http.StatusNotFound, errors.New("post is not found"))
		return
	}

	if post.Title == request.Title && post.Content == request.Content {
		// Is not changed
		response.SetStatusCode(http.StatusOK).
			WriteJSON(ctx)
		return
	}

	post.Title = request.Title
	post.Content = request.Content

	if err := h.postRepo.Update(ctx, post); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if err := h.invalidateUserCaches(ctx, userID); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)
}

// Delete route
func (h *Handler) Delete(ctx *gin.Context) {
	request := NewDeletePostRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateDeletePostRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	post, err := h.postRepo.Get(ctx, request.ID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if post == nil {
		response.Abort(ctx, http.StatusNotFound, errors.New("post is not found"))
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	if post.CreatedBy != userID {
		response.Abort(ctx, http.StatusNotFound, errors.New("post is not found"))
		return
	}

	if err := h.postRepo.Delete(ctx, post.ID); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if err := h.invalidateUserCaches(ctx, userID); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)
}

func (h *Handler) invalidateUserCaches(ctx context.Context, userID int64) error {
	followerIDs, err := h.followerRepo.GetFollowByList(ctx, userID, 0, 0)
	if err != nil {
		return err
	}

	h.userFeedCacheInvalidator.InvalidateEntries(followerIDs)
	return nil
}
