package posts

import (
	"errors"

	"github.com/gin-gonic/gin"
)

type (
	CreatePostRequest struct {
		Title   string `json:"title"`
		Content string `json:"content"`
	}

	UpdatePostRequest struct {
		ID      int64  `json:"id"`
		Title   string `json:"title"`
		Content string `json:"content"`
	}

	DeletePostRequest struct {
		ID int64 `json:"id"`
	}
)

func NewCreatePostRequest() *CreatePostRequest {
	return &CreatePostRequest{}
}

func NewUpdatePostRequest() *UpdatePostRequest {
	return &UpdatePostRequest{}
}

func NewDeletePostRequest() *DeletePostRequest {
	return &DeletePostRequest{}
}

func (r *CreatePostRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func (r *UpdatePostRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func (r *DeletePostRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func ValidateCreatePostRequest(_ *gin.Context, request *CreatePostRequest) error {
	if len(request.Title) == 0 {
		return errors.New("title is not specified")
	}

	if len(request.Content) == 0 {
		return errors.New("content is not specified")
	}

	return nil
}

func ValidateUpdatePostRequest(_ *gin.Context, request *UpdatePostRequest) error {
	if request.ID <= 0 {
		return errors.New("id must be greater than zero")
	}

	if len(request.Title) == 0 {
		return errors.New("title is not specified")
	}

	if len(request.Content) == 0 {
		return errors.New("content is not specified")
	}

	return nil
}

func ValidateDeletePostRequest(_ *gin.Context, request *DeletePostRequest) error {
	if request.ID <= 0 {
		return errors.New("id must be greater than zero")
	}

	return nil
}
