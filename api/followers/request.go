package followers

import (
	"errors"

	"github.com/gin-gonic/gin"
)

type (
	FollowRequest struct {
		FollowTo int64 `json:"follow_to"`
	}
)

func NewFollowRequest() *FollowRequest {
	return &FollowRequest{}
}

func (r *FollowRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func ValidateFollowRequest(_ *gin.Context, request *FollowRequest) error {
	if request.FollowTo <= 0 {
		return errors.New("user id must be greater than zero")
	}

	return nil
}
