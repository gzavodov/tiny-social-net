package followers

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/caches"

	"gitlab.com/gzavodov/tiny-social-net/api/responses"

	"gitlab.com/gzavodov/tiny-social-net/api/requests"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	followerRepo             domain.FollowerRepository
	userRepo                 domain.UserRepository
	userAuthInfoProvider     security.UserAuthInfoProvider
	userFeedCacheInvalidator caches.UserFeedCacheInvalidator
}

func NewHandler(
	followerRepo domain.FollowerRepository,
	userRepo domain.UserRepository,
	userAuthInfoProvider security.UserAuthInfoProvider,
	userFeedCacheInvalidator caches.UserFeedCacheInvalidator,
) *Handler {
	return &Handler{
		followerRepo:             followerRepo,
		userRepo:                 userRepo,
		userAuthInfoProvider:     userAuthInfoProvider,
		userFeedCacheInvalidator: userFeedCacheInvalidator,
	}
}

// Follow route
func (h *Handler) Follow(ctx *gin.Context) {
	request := NewFollowRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateFollowRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	isUserExists, err := h.userRepo.IsExists(ctx, request.FollowTo)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if !isUserExists {
		response.Abort(ctx, http.StatusBadRequest, fmt.Errorf("user with id '%d' does not exists", request.FollowTo))
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()

	isExists, err := h.followerRepo.IsExists(ctx, userID, request.FollowTo)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if isExists {
		response.Abort(ctx, http.StatusBadRequest, fmt.Errorf("user with id '%d' is already followed", request.FollowTo))
		return
	}

	if err := h.followerRepo.Create(ctx, userID, request.FollowTo, time.Now().UTC()); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	h.userFeedCacheInvalidator.InvalidateEntries([]int64{userID})

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)
}

// Unfollow route
func (h *Handler) Unfollow(ctx *gin.Context) {
	request := NewFollowRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateFollowRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()

	if err := h.followerRepo.Delete(ctx, userID, request.FollowTo); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	h.userFeedCacheInvalidator.InvalidateEntries([]int64{userID})

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)

}

func (h *Handler) GetFollowByList(ctx *gin.Context) {
	response := responses.New()

	userID, err := requests.GetIntegerURLParam(ctx, "id")
	if err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if userID <= 0 {
		userID = h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	}

	if userID <= 0 {
		response.Abort(ctx, http.StatusBadRequest, errors.New("can't resolve user ID"))
		return
	}

	followerIDs, err := h.followerRepo.GetFollowByList(ctx, userID, 0, 0)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(
			gin.H{
				"user_id":            userID,
				"follow_by_user_ids": followerIDs,
			},
		).
		WriteJSON(ctx)
}

func (h *Handler) GetFollowToList(ctx *gin.Context) {
	response := responses.New()

	userID, err := requests.GetIntegerURLParam(ctx, "id")
	if err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if userID <= 0 {
		userID = h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	}

	if userID <= 0 {
		response.Abort(ctx, http.StatusBadRequest, errors.New("can't resolve user ID"))
		return
	}

	followingIDs, err := h.followerRepo.GetFollowToList(ctx, userID, 0, 0)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(
			gin.H{
				"user_id":            userID,
				"follow_to_user_ids": followingIDs,
			},
		).
		WriteJSON(ctx)
}
