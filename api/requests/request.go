package requests

import (
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

var ErrParamNotFound = errors.New("request parameter is not found")

func GetIntegerURLParam(ctx *gin.Context, name string) (int64, error) {
	str := ctx.Param(name)
	if str == "" {
		return 0, ErrParamNotFound
	}

	var (
		value int64
		err   error
	)
	value, err = strconv.ParseInt(str, 10, 64)
	if err != nil {
		return 0, err
	}

	return value, nil
}
