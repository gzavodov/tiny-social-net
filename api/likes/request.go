package likes

import (
	"errors"

	"github.com/gin-gonic/gin"
)

type (
	LikeRequest struct {
		PostID int64 `json:"post_id"`
	}
)

func NewLikeRequest() *LikeRequest {
	return &LikeRequest{}
}

func (r *LikeRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func ValidateLikeRequest(_ *gin.Context, request *LikeRequest) error {
	if request.PostID <= 0 {
		return errors.New("post_id must be greater than zero")
	}

	return nil
}
