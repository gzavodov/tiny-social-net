package likes

import (
	"net/http"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/api/requests"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/api/responses"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	likeRepo             domain.LikeRepository
	userAuthInfoProvider security.UserAuthInfoProvider
}

func NewHandler(
	likeRepo domain.LikeRepository,
	userAuthInfoProvider security.UserAuthInfoProvider,
) *Handler {
	return &Handler{likeRepo: likeRepo, userAuthInfoProvider: userAuthInfoProvider}
}

// Add route
func (h *Handler) Add(ctx *gin.Context) {
	request := NewLikeRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateLikeRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	isLiked, err := h.likeRepo.IsLiked(ctx, request.PostID, userID)
	if err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if isLiked {
		// Already liked
		response.SetStatusCode(http.StatusOK).
			WriteJSON(ctx)
		return
	}

	if err := h.likeRepo.Like(ctx, request.PostID, userID, time.Now().UTC()); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)
}

// Remove route
func (h *Handler) Remove(ctx *gin.Context) {
	request := NewLikeRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateLikeRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	userID := h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	if err := h.likeRepo.UnLike(ctx, request.PostID, userID); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		WriteJSON(ctx)
}

// GetCount route
func (h *Handler) GetCount(ctx *gin.Context) {
	response := responses.New()

	postID, err := requests.GetIntegerURLParam(ctx, "id")
	if err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	count, err := h.likeRepo.GetCount(ctx, postID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(
			gin.H{
				"post_id": postID,
				"count":   count,
			},
		).
		WriteJSON(ctx)
}
