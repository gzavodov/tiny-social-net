package caches

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/allegro/bigcache/v3"
)

type (
	NewsFeedCache struct {
		cache       *bigcache.BigCache
		invalidator userCacheInvalidator
	}

	NewsFeedCacheOption func(*NewsFeedCache)

	userCacheInvalidator interface {
		InvalidateEntries(cache *bigcache.BigCache, userIDs []int64)
	}

	defaultUserCacheInvalidator struct {
	}

	batchUserCacheInvalidator struct {
		BatchSize uint16
		Delay     time.Duration
	}
)

func NewNewsFeed(cache *bigcache.BigCache, opts ...NewsFeedCacheOption) *NewsFeedCache {
	f := &NewsFeedCache{cache: cache}

	for _, opt := range opts {
		opt(f)
	}

	if f.invalidator == nil {
		f.invalidator = &defaultUserCacheInvalidator{}
	}

	return f
}

func WithBatchInvalidation(batchSize uint16, delay time.Duration) NewsFeedCacheOption {
	return func(h *NewsFeedCache) {
		h.invalidator = &batchUserCacheInvalidator{BatchSize: batchSize, Delay: delay}
	}
}

func (f *NewsFeedCache) GetEntry(userID int64) ([]byte, error) {
	entry, err := f.cache.Get(prepareNewsFeedKey(userID))
	if err != nil && !errors.Is(err, bigcache.ErrEntryNotFound) {
		return nil, err
	}

	return entry, nil
}

func (f *NewsFeedCache) SetEntry(userID int64, entry []byte) error {
	return f.cache.Set(prepareNewsFeedKey(userID), entry)
}

func (f *NewsFeedCache) InvalidateEntries(userIDs []int64) {
	if len(userIDs) == 0 {
		return
	}

	f.invalidator.InvalidateEntries(f.cache, userIDs)
}

// InvalidateEntries - сбрасывает кеш пользователей
func (ci *defaultUserCacheInvalidator) InvalidateEntries(cache *bigcache.BigCache, userIDs []int64) {
	if len(userIDs) == 0 {
		return
	}

	for _, id := range userIDs {
		var key = prepareNewsFeedKey(id)
		err := cache.Delete(key)
		if err != nil && !errors.Is(err, bigcache.ErrEntryNotFound) {
			log.Printf("failed to invalidate cache entry (%s): %v\n", key, err)
		}
	}
}

// InvalidateEntries - сбрасывает кеш пользователей
// Пользователи делятся на группы, которые обрабатываются с нарастающей задержкой.
// Обеспечивает ступенчатый сброс кеша для предотвращения лавинообразного перестроения кеша.
func (ci *batchUserCacheInvalidator) InvalidateEntries(cache *bigcache.BigCache, userIDs []int64) {
	if len(userIDs) == 0 {
		return
	}

	var (
		count = len(userIDs)

		start = 0
		end   = int(ci.BatchSize)

		step = 0
	)

	for {
		if end > count {
			end = count
		}

		if end-start <= 0 {
			break
		}

		step++
		batch := userIDs[start:end]

		go func(batch []int64, delay time.Duration, cache *bigcache.BigCache) {
			select {
			case <-time.After(delay):
				for _, id := range batch {
					var key = prepareNewsFeedKey(id)
					err := cache.Delete(key)
					if err != nil && !errors.Is(err, bigcache.ErrEntryNotFound) {
						log.Printf("failed to invalidate cache entry (%s): %v\n", key, err)
					}
				}
			}
		}(batch, time.Duration(step)*ci.Delay, cache)

		start = end
		end += int(ci.BatchSize)
	}
}

func prepareNewsFeedKey(userID int64) string {
	return fmt.Sprintf("news-feed-user[%d]", userID)
}
