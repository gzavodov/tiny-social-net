package middlewares

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type AuthenticationHandler struct {
	authToken *security.JWT
}

func NewAuthenticationHandler(authToken *security.JWT) *AuthenticationHandler {
	return &AuthenticationHandler{authToken: authToken}
}

func (h *AuthenticationHandler) EnsureLoggedIn(ctx *gin.Context) {
	claims, err := h.authToken.Parse(ctx)
	if err != nil {
		switch {
		case errors.Is(err, security.ErrUnauthorized),
			errors.Is(err, security.ErrInvalidToken):
			ctx.JSON(http.StatusUnauthorized, gin.H{"error": err.Error()})
		default:
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		}

		ctx.Abort()
		return
	}

	h.authToken.SetJWTClaims(ctx, claims)
	ctx.Next()
}

func (h *AuthenticationHandler) EnsureNotLoggedIn(ctx *gin.Context) {
	_, err := h.authToken.Parse(ctx)
	if err != nil {
		if !errors.Is(err, security.ErrUnauthorized) && !errors.Is(err, security.ErrInvalidToken) {
			ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			ctx.Abort()
			return
		}

		ctx.Next()
		return
	}

	ctx.JSON(http.StatusForbidden, gin.H{"error": "request is only allowed for unauthorized users"})
	ctx.Abort()
	return
}
