package responses

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

var JSONContentType = []string{"application/json; charset=utf-8"}

type Response struct {
	code        int
	fields      gin.H
	messageList []string
	errorList   []error
}

func New() *Response {
	return &Response{
		fields: make(gin.H),
	}
}

func (r *Response) AddField(name string, value interface{}) *Response {
	r.fields[name] = value
	return r
}

func (r *Response) SetFields(fields gin.H) *Response {
	r.fields = fields
	return r
}

func (r *Response) AddMessage(msg string) *Response {
	r.messageList = append(r.messageList, msg)
	return r
}

func (r *Response) AddError(err error) *Response {
	if err == nil {
		return r
	}

	r.errorList = append(r.errorList, err)
	return r
}

func (r *Response) SetError(err error) *Response {
	if err == nil {
		if len(r.errorList) > 0 {
			r.errorList = nil
		}
		return r
	}

	r.errorList = []error{err}
	return r
}

func (r *Response) SetStatusCode(code int) *Response {
	r.code = code
	return r
}

func (r *Response) WriteJSON(c *gin.Context) *Response {
	var code = r.code
	if code == 0 {
		code = http.StatusOK
	}

	c.JSON(code, r.prepareData())
	return r
}

func (r *Response) Write(c *gin.Context, contentType []string, content []byte) *Response {
	var code = r.code
	if code == 0 {
		code = http.StatusOK
	}

	c.Render(code, RawDataRender{contentType: contentType, content: content})
	return r
}

func (r *Response) Abort(c *gin.Context, code int, err error) {
	r.SetError(err).SetStatusCode(code).WriteJSON(c)
	c.AbortWithStatus(code)
}

func PrepareData(fields gin.H) map[string]interface{} {
	data := make(map[string]interface{})
	for k, v := range fields {
		data[k] = v
	}

	data["success"] = true

	return data
}

func (r *Response) prepareData() map[string]interface{} {
	data := make(map[string]interface{})
	for k, v := range r.fields {
		data[k] = v
	}

	data["success"] = true
	if len(r.messageList) > 0 {
		data["messages"] = strings.Join(r.messageList, "\n")
	}

	if len(r.errorList) > 0 {
		data["success"] = false

		errs := make([]string, 0, len(r.errorList))
		for _, err := range r.errorList {
			errs = append(errs, err.Error())
		}

		data["errors"] = strings.Join(errs, "\n")
	}

	return data
}

type RawDataRender struct {
	contentType []string
	content     []byte
}

func (r RawDataRender) Render(w http.ResponseWriter) error {
	r.WriteContentType(w)
	_, err := w.Write(r.content)
	return err
}

func (r RawDataRender) WriteContentType(w http.ResponseWriter) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = r.contentType
	}
}
