package feeds

import (
	"encoding/json"
	"net/http"

	"gitlab.com/gzavodov/tiny-social-net/caches"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/api/responses"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	userRepo              domain.UserRepository
	userAuthInfoProvider  security.UserAuthInfoProvider
	userFeedCacheProvider caches.UserFeedCacheProvider
	postRepo              domain.PostRepository
}

func NewHandler(
	userRepo domain.UserRepository,
	userAuthInfoProvider security.UserAuthInfoProvider,
	userFeedCacheProvider caches.UserFeedCacheProvider,
	postRepo domain.PostRepository,
) *Handler {
	return &Handler{
		userRepo:              userRepo,
		userAuthInfoProvider:  userAuthInfoProvider,
		userFeedCacheProvider: userFeedCacheProvider,
		postRepo:              postRepo,
	}
}

func (h *Handler) News(ctx *gin.Context) {
	var (
		response = responses.New()
		userID   = h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
		content  []byte
		err      error
	)

	content, err = h.userFeedCacheProvider.GetEntry(userID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if content != nil {
		response.SetStatusCode(http.StatusOK).Write(ctx, responses.JSONContentType, content)
		return
	}

	posts, err := h.postRepo.GetFollowingList(ctx, userID, 1000, 0)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	authorIDs := make([]int64, 0, len(posts))
	for _, post := range posts {
		authorIDs = append(authorIDs, post.CreatedBy)
	}

	authorLogins, err := h.userRepo.GetLoginByIDs(ctx, authorIDs)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	feeds := make([]interface{}, 0, len(posts))
	for _, post := range posts {
		feeds = append(feeds,
			map[string]interface{}{
				"postID":            post.ID,
				"title":             post.Title,
				"content":           post.Content,
				"createdBy":         post.CreatedBy,
				"createdByUsername": authorLogins[post.CreatedBy],
				"createdAt":         post.CreatedAt,
			},
		)
	}

	content, err = json.Marshal(
		responses.PrepareData(
			gin.H{
				"user_id": userID,
				"feed":    feeds,
			},
		),
	)

	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if err = h.userFeedCacheProvider.SetEntry(userID, content); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).Write(ctx, responses.JSONContentType, content)
}
