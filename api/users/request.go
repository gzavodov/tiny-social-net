package users

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
)

type (
	SignupRequest struct {
		Login         string `json:"login"`
		Email         string `json:"email"`
		Password      string `json:"password"`
		PasswordAgain string `json:"password_again"`
	}

	LoginRequest struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}
)

func NewSignupRequest() *SignupRequest {
	return &SignupRequest{}
}

func (r *SignupRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func NewLoginRequest() *LoginRequest {
	return &LoginRequest{}
}

func (r *LoginRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func ValidateSignupRequest(ctx *gin.Context, request *SignupRequest, userRepo domain.UserRepository) error {
	if len(request.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(request.Login) < 4 || len(request.Login) > 32 {
		return errors.New("login should be between 4 and 32")
	}

	if len(request.Password) == 0 {
		return errors.New("password is not specified")
	}

	if len(request.PasswordAgain) == 0 {
		return errors.New("password_again is not specified")
	}

	if request.Password != request.PasswordAgain {
		return errors.New("passwords don't match")
	}

	//mailErr := checkmail.ValidateFormat(email)
	if len(request.Email) == 0 {
		return errors.New("email is not specified")
	}

	isLoginExists, err := userRepo.IsLoginExists(ctx, request.Login)
	if err != nil {
		return err
	}

	if isLoginExists {
		return errors.New("login already exists")
	}

	isEmailAddressExists, err := userRepo.IsEmailAddressExists(ctx, request.Email)
	if err != nil {
		return err
	}

	if isEmailAddressExists {
		return errors.New("email already exists")
	}

	return nil
}

func ValidateLoginRequest(_ *gin.Context, request *LoginRequest) error {
	if len(request.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(request.Password) == 0 {
		return errors.New("password is not specified")
	}

	return nil
}
