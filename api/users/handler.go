package users

import (
	"errors"
	"net/http"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/api/responses"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	authToken *security.JWT
	userRepo  domain.UserRepository
}

func NewHandler(authToken *security.JWT, userRepo domain.UserRepository) *Handler {
	return &Handler{authToken: authToken, userRepo: userRepo}
}

// Signup function to register user
func (h *Handler) Signup(ctx *gin.Context) {
	request := NewSignupRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateSignupRequest(ctx, request, h.userRepo); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	passwordHash, err := domain.GeneratePasswordHash(request.Password)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	user := &domain.User{
		Login:    request.Login,
		Email:    request.Email,
		Password: passwordHash,
		Joined:   time.Now().UTC(),
	}

	if err := h.userRepo.Create(ctx, user); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusCreated).
		SetFields(gin.H{"id": user.ID}).
		WriteJSON(ctx)
}

// Login function to log user in
func (h *Handler) Login(ctx *gin.Context) {
	request := NewLoginRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateLoginRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	user, err := h.userRepo.GetByLogin(ctx, request.Login)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	if user == nil || !domain.CheckPasswordHash(request.Password, user.Password) {
		response.Abort(
			ctx,
			http.StatusUnauthorized,
			errors.New("provided credentials are not valid"),
		)
		return
	}

	token, err := h.authToken.Generate(user.ID, user.Login, user.Email)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(gin.H{
			"id":    user.ID,
			"token": token,
		}).
		WriteJSON(ctx)
}

// Find function to find users by name
func (h *Handler) Find(ctx *gin.Context) {
	var filter *domain.Filter
	queryFilter, _ := ctx.GetQueryMap("filter")
	fields := make([]*domain.Field, 0, 2)
	firstName, ok := queryFilter["firstName"]
	if ok {
		fields = append(fields, &domain.Field{Name: "firstName", Value: firstName, Operator: domain.FilterOperatorStartsWith})
	}

	lastName, ok := queryFilter["lastName"]
	if ok {
		fields = append(fields, &domain.Field{Name: "lastName", Value: lastName, Operator: domain.FilterOperatorStartsWith})
	}

	if len(fields) > 0 {
		filter = &domain.Filter{Fields: fields, Logic: domain.FilterLogicAnd}
	}

	response := responses.New()

	users, err := h.userRepo.GetList(ctx, filter, 0, 0)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(gin.H{
			"users":  users,
			"filter": queryFilter,
		}).
		WriteJSON(ctx)
}
