package profiles

import (
	"errors"
	"fmt"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/badoux/checkmail"

	"github.com/gin-gonic/gin"
)

type (
	ProfileUpdateRequest struct {
		Login      string        `json:"login"`
		FirstName  string        `json:"first_name"`
		LastName   string        `json:"last_name"`
		Email      string        `json:"email"`
		Residence  string        `json:"residence"`
		DayOfBirth time.Time     `json:"dateOfBirth"`
		Gender     domain.Gender `json:"gender"`
		Bio        string        `json:"bio"`
	}
)

func NewProfileUpdateRequest() *ProfileUpdateRequest {
	return &ProfileUpdateRequest{}
}

func (r *ProfileUpdateRequest) InitializeFromJSON(ctx *gin.Context) error {
	return ctx.ShouldBindJSON(r)
}

func ValidateProfileUpdateRequest(_ *gin.Context, request *ProfileUpdateRequest) error {
	if len(request.Login) == 0 {
		return errors.New("login is not specified")
	}

	if len(request.Email) == 0 {
		return errors.New("email is not specified")
	}

	if err := checkmail.ValidateFormat(request.Email); err != nil {
		return fmt.Errorf("email: %w", err)
	}

	return nil
}
