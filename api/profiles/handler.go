package profiles

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/gzavodov/tiny-social-net/api/responses"

	"gitlab.com/gzavodov/tiny-social-net/api/requests"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/domain"
	"gitlab.com/gzavodov/tiny-social-net/security"
)

type Handler struct {
	userRepo             domain.UserRepository
	userAuthInfoProvider security.UserAuthInfoProvider
}

func NewHandler(
	userRepo domain.UserRepository,
	userAuthInfoProvider security.UserAuthInfoProvider,
) *Handler {
	return &Handler{
		userAuthInfoProvider: userAuthInfoProvider,
		userRepo:             userRepo,
	}
}

func (h *Handler) Get(ctx *gin.Context) {
	response := responses.New()

	var userID int64

	userID, err := requests.GetIntegerURLParam(ctx, "id")
	if err != nil {
		if !errors.Is(err, requests.ErrParamNotFound) {
			response.Abort(ctx, http.StatusBadRequest, err)
			return
		}

		userID = h.userAuthInfoProvider.GetAuthInfo(ctx).GetID()
	}

	if userID <= 0 {
		response.Abort(ctx, http.StatusBadRequest, errors.New("can't resolve user ID"))
		return
	}

	user, err := h.userRepo.Get(ctx, userID)
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, err)
		return
	}

	response.SetStatusCode(http.StatusOK).
		SetFields(
			gin.H{
				"id":          strconv.FormatInt(user.ID, 10),
				"login":       user.Login,
				"first_name":  user.FirstName,
				"last_name":   user.LastName,
				"email":       user.Email,
				"residence":   user.Residence,
				"dateOfBirth": user.DayOfBirth,
				"gender":      user.Gender,
				"bio":         user.Bio,
			},
		).
		WriteJSON(ctx)
}

func (h *Handler) Update(ctx *gin.Context) {
	request := NewProfileUpdateRequest()
	response := responses.New()

	if err := request.InitializeFromJSON(ctx); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	if err := ValidateProfileUpdateRequest(ctx, request); err != nil {
		response.Abort(ctx, http.StatusBadRequest, err)
		return
	}

	user, err := h.userRepo.Get(ctx, h.userAuthInfoProvider.GetAuthInfo(ctx).GetID())
	if err != nil {
		response.Abort(ctx, http.StatusInternalServerError, fmt.Errorf("failed to get user: %w", err))
		return
	}

	isChanged := false
	if user.Login != request.Login {
		user.Login = request.Login
		isChanged = true
	}

	if user.FirstName != request.FirstName {
		user.FirstName = request.FirstName
		isChanged = true
	}

	if user.LastName != request.LastName {
		user.LastName = request.LastName
		isChanged = true
	}

	if user.Email != request.Email {
		user.Email = request.Email
		isChanged = true
	}

	if user.Residence != request.Residence {
		user.Residence = request.Residence
		isChanged = true
	}

	if user.DayOfBirth != request.DayOfBirth {
		user.DayOfBirth = request.DayOfBirth
		isChanged = true
	}

	if user.Gender != request.Gender {
		user.Gender = request.Gender
		isChanged = true
	}

	if user.Bio != request.Bio {
		user.Bio = request.Bio
		isChanged = true
	}

	if !isChanged {
		return
	}

	if err := h.userRepo.Update(ctx, user); err != nil {
		response.Abort(ctx, http.StatusInternalServerError, fmt.Errorf("failed to update user: %w", err))
		return
	}

	response.SetStatusCode(http.StatusOK)
}
