package domain

type (
	FilterOperator string
	FilterLogic    string
)

const (
	FilterOperatorStartsWith     FilterOperator = "StartsWith"
	FilterOperatorEquals         FilterOperator = "Equals"
	FilterOperatorLessThan       FilterOperator = "LessThan"
	FilterOperatorLessOrEqual    FilterOperator = "LessOrEqual"
	FilterOperatorGreaterThan    FilterOperator = "GreaterThan"
	FilterOperatorGreaterOrEqual FilterOperator = "GreaterOrEqual"

	FilterLogicAnd FilterLogic = "AND"
	FilterLogicOr  FilterLogic = "OR"
)

type (
	Field struct {
		Name     string
		Value    string
		Operator FilterOperator
	}

	Filter struct {
		Fields []*Field
		Logic  FilterLogic
	}
)
