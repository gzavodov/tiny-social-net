package domain

import (
	"context"
	"time"
)

type (
	ProfileViewRepository interface {
		Create(ctx context.Context, viewBy int64, viewTo int64, viewTime time.Time) error
		GetCount(ctx context.Context, userIDs []int64) (map[int64]int64, error)
	}
)
