package domain

import (
	"context"
	"time"
)

type (
	Post struct {
		ID        int64
		Title     string
		Content   string
		CreatedBy int64
		CreatedAt time.Time
	}

	PostRepository interface {
		Create(context.Context, *Post) error
		Update(context.Context, *Post) error
		Delete(context.Context, int64) error

		Get(context.Context, int64) (*Post, error)
		GetList(ctx context.Context, createdBy int64, limit int, offset int) ([]*Post, error)
		GetFollowingList(ctx context.Context, followBy int64, limit int, offset int) ([]*Post, error)
	}
)
