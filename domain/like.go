package domain

import (
	"context"
	"time"
)

type (
	LikeRepository interface {
		Like(ctx context.Context, postID int64, likeBy int64, likeTime time.Time) error
		UnLike(ctx context.Context, postID int64, likeBy int64) error
		IsLiked(ctx context.Context, postID int64, likeBy int64) (bool, error)
		GetCount(ctx context.Context, postID int64) (uint32, error)
	}
)
