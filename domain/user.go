package domain

import (
	"context"
	"errors"
	"time"

	"golang.org/x/crypto/bcrypt"
)

type Gender string

const (
	UnknownGender Gender = ""
	MaleGender    Gender = "M"
	FemaleGender  Gender = "F"
)

var (
	ErrDuplicateRecord = errors.New("duplicate record")
)

type (
	User struct {
		ID         int64
		Login      string
		FirstName  string
		LastName   string
		Email      string
		Password   string
		Residence  string
		DayOfBirth time.Time
		Gender     Gender
		Bio        string
		Joined     time.Time
	}

	UserRepository interface {
		Create(context.Context, *User) error
		Update(context.Context, *User) error
		Delete(context.Context, int64) error

		Get(context.Context, int64) (*User, error)
		GetByLogin(ctx context.Context, login string) (*User, error)
		GetList(ctx context.Context, filter *Filter, limit int, offset int) ([]*User, error)
		GetListByIDs(ctx context.Context, userIDs []int64) ([]*User, error)
		GetLoginByIDs(ctx context.Context, userIDs []int64) (map[int64]string, error)

		IsExists(ctx context.Context, int642 int64) (bool, error)
		IsLoginExists(ctx context.Context, email string) (bool, error)
		IsEmailAddressExists(ctx context.Context, email string) (bool, error)
	}
)

func GeneratePasswordHash(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}

func CheckPasswordHash(password, hash string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password)) == nil
}

func (g Gender) String() string {
	return string(g)
}
