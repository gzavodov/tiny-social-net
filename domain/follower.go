package domain

import (
	"context"
	"time"
)

type (
	FollowerRepository interface {
		Create(ctx context.Context, followBy, followTo int64, followTime time.Time) error
		Delete(context.Context, int64, int64) error
		IsExists(ctx context.Context, followBy, followTo int64) (bool, error)

		GetFollowByList(ctx context.Context, followTo int64, limit int, offset int) ([]int64, error)
		GetFollowToList(ctx context.Context, followBy int64, limit int, offset int) ([]int64, error)

		GetFollowByCount(context.Context, []int64) (map[int64]int64, error)
		GetFollowToCount(context.Context, []int64) (map[int64]int64, error)
	}
)
