# tiny-social-net

Small social network. An optimized fork of Go-Mini-Social-Network

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/gzavodov/tiny-social-net.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/gzavodov/tiny-social-net/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Name
Tiny-social-net

## Usage
#### RU
1. Клонируем репозиторий
```
$ git clone https://gitlab.com/gzavodov/tiny-social-net.git
```
3. Переходим в директорию проекта
``` 
$ cd ./tiny-social-net
``` 
5. Создаём ключ шифрования сессий пользователя и токенов JWT
``` 
$ make secret
```   
   В результате выполнения команды должен быть создан файл ```./env/api-secret.env```
   Если что пошло не так, то можно создать секрет руками примерно с таким содержанием:
```
TINY_SOCIAL_NET_SESSION_SECRET=<Секрет>
```   
7. Запускаем проект
``` 
$ make up
```   
   После окончания сборки и запуска открываем проект.

### REST-API
   Доступен по адресу ```http://localhost:9090/api/v2```
   Импортируем коллекцию tiny-social-net
   Для работы автоматизации тестирования нужно чтобы было активно любое окружение Postman (Environment)
   https://learning.postman.com/docs/sending-requests/managing-environments/#creating-environments

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/85eecbd3eb2ebf4f64d2#?env%5Btiny-social-net%5D=W3sia2V5IjoiaG9zdCIsInZhbHVlIjoibG9jYWxob3N0OjkwOTAiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6InBvc3RfaWQiLCJ2YWx1ZSI6IiIsImVuYWJsZWQiOnRydWV9LHsia2V5IjoicHJldmlvdXNfdXNlcl9pZCIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZX0seyJrZXkiOiJ1c2VyX2lkIiwidmFsdWUiOiIiLCJlbmFibGVkIjp0cnVlfSx7ImtleSI6Imp3dCIsInZhbHVlIjoiIiwiZW5hYmxlZCI6dHJ1ZX1d)

#### Примерный сценарий тестирования
1. Регистрируем первого пользователя запросом ``user/signup [Artemio Lombardo]``
2. Аутентифицируем первого пользователя ``user/login [Artemio Lombardo]``
   Полученный JWT автоматически сохранится в окружении Postman и последующие запросы будут отправлять его в заголовке Authorization.
3. Выполняем обновление профиля пользователя, чтобы заполнить персональную информацию (ФИО, пол, дату рождения, город и интересы) ``profile/update [Artemio Lombardo]``
4. Проверяем профиль пользователя, чтобы убедиться в сохранении данных (опционально) ``profile/view`` (текущий пользователь будет автоматом найден на беке по JWT)
5. Регистрируем второго пользователя запросом ``user/signup [Marcel Bürger]``
6. Аутентифицируем второго пользователя ``user/login [Marcel Bürger]``
7. Добавляем второму пользователю в фолловеры первого ``follower/follow``
   Идентификатор первого пользователя был сохранён в окружении Postman и автоматом отправится в запросе
8. Проверяем, что второй пользователь следит за первым ``follower/follow-to/list``
   В поле ``follow_to_user_ids`` должен прийти ``id`` первого пользователя.

### WEB интерфейс
Открываем в браузере сайт http:://localhost:9090, можно регистрироваться, писать посты и т.д.
- [Регистрация](http://localhost:9090/signup)
- [Логин](http://localhost:9090/login)
- [Список пользователей](http://localhost:9090/explore)
- [Просмотр профиля пользователя](http://localhost:9090/profile/view)
- [Просмотр списка фолловеров](http://localhost:9090/followers)
- [Создание поста](http://localhost:9090/create_post)

По завершении работы выполняем
```
$ make down
```
Эта команда удалит локальные имиджи.

## License
MIT License (MIT)

## Project status
Under development
