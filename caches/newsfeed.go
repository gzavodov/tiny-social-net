package caches

type UserFeedCacheProvider interface {
	GetEntry(userID int64) ([]byte, error)
	SetEntry(userID int64, entry []byte) error
}

type UserFeedCacheInvalidator interface {
	InvalidateEntries(userIDs []int64)
}
