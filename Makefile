secret:
	cat /dev/urandom | tr -dc '[:alpha:][:digit:]' | fold --width $${1:-64} | head -n 1 | sed 's/^/TINY_SOCIAL_NET_SESSION_SECRET=/' | tee ./env/api-secret.env
build:
	docker-compose -f ./docker-compose.yml build --parallel
up:
	docker-compose -f ./docker-compose.yml up --attach --no-recreate
down:
	docker-compose -f ./docker-compose.yml down --rmi local