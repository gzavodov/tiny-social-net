package main

import (
	"context"
	"encoding/csv"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/internal/database/balancers"

	"gitlab.com/gzavodov/tiny-social-net/internal/database/mysql"

	"gitlab.com/gzavodov/tiny-social-net/domain"

	"github.com/jmoiron/sqlx"

	"gitlab.com/gzavodov/tiny-social-net/config"
)

func main() {
	configFilePath := flag.String("config", "", "Path to configuration file")
	flag.Parse()

	conf := &config.Configuration{}
	err := conf.Load(
		*configFilePath,
		&config.Configuration{},
	)
	if err != nil {
		log.Fatalf("could not load configuration: %v", err)
	}

	masterDbConn := sqlx.MustConnect("mysql", conf.DSN)
	if err := masterDbConn.Ping(); err != nil {
		log.Fatal(err)
	}

	defer masterDbConn.Close()

	loadBalancer := balancers.NewLoadBalancer(masterDbConn, nil)
	userRepo := mysql.NewUserRepository(loadBalancer)

	sourceDataPath := "./testify/user/"
	files, err := ioutil.ReadDir(sourceDataPath)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	wg := &sync.WaitGroup{}

	for _, file := range files {
		if file.IsDir() {
			continue
		}

		fileName := file.Name()
		if filepath.Ext(fileName) != ".csv" {
			continue
		}

		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()

			filePath := fmt.Sprintf("%s%s", sourceDataPath, fileName)
			err := processUserDataFile(ctx, filePath, userRepo)
			if err != nil {
				fmt.Printf("failed to process data file (%s): %s\n", filePath, err.Error())
				return
			}

			fmt.Printf("data file is processed (%s)\n", filePath)
		}(wg)
	}

	wg.Wait()
}

func processUserDataFile(ctx context.Context, filePath string, userRepo domain.UserRepository) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	defer file.Close()

	csvReader := csv.NewReader(file)
	csvReader.LazyQuotes = true

	data, err := csvReader.ReadAll()
	if err != nil {
		return err
	}

	processUserData(ctx, data, userRepo)

	return nil
}

func processUserData(ctx context.Context, data [][]string, userRepo domain.UserRepository) {
	for i, item := range data {
		if i == 0 {
			continue
		}

		firstName := item[0]
		lastName := item[1]
		residence := item[2]
		email := item[3]

		if len(email) == 0 {
			continue
		}

		gender := domain.MaleGender
		if item[4] == "female" {
			gender = domain.FemaleGender
		}

		dayOfBirth, err := time.Parse("1/2/2006", item[4])
		if err != nil {
			fmt.Printf("failed to parse date (%s): %s\n", item[4], err.Error())
			continue
		}

		passwordHash, err := domain.GeneratePasswordHash(email)
		if err != nil {
			fmt.Printf("failed to generate password hash: %s\n", err.Error())
			continue
		}

		login := strings.ToLower(email)

		user := &domain.User{
			FirstName:  firstName,
			LastName:   lastName,
			Residence:  residence,
			Email:      email,
			DayOfBirth: dayOfBirth,
			Login:      login,
			Password:   passwordHash,
			Gender:     gender,
			Joined:     time.Now().UTC(),
		}

		if err := userRepo.Create(ctx, user); err != nil {
			if errors.Is(err, domain.ErrDuplicateRecord) {
				fmt.Printf("user already exists: %s\n", login)
				continue
			}

			fmt.Printf("failed to create user(%s) : %s\n", user.Login, err.Error())
			continue
		}

		fmt.Printf("%d %s\n", user.ID, user.Login)
	}
}
