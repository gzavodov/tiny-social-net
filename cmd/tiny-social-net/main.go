package main

import (
	"flag"
	"log"
	"time"

	"gitlab.com/gzavodov/tiny-social-net/api/caches"

	"github.com/allegro/bigcache/v3"

	"gitlab.com/gzavodov/tiny-social-net/api/feeds"

	"gitlab.com/gzavodov/tiny-social-net/internal/database/balancers"

	"github.com/jmoiron/sqlx"

	"gitlab.com/gzavodov/tiny-social-net/api/likes"

	"gitlab.com/gzavodov/tiny-social-net/api/posts"

	"gitlab.com/gzavodov/tiny-social-net/api/followers"

	"gitlab.com/gzavodov/tiny-social-net/api/profiles"

	api_middlewares "gitlab.com/gzavodov/tiny-social-net/api/middlewares"

	"gitlab.com/gzavodov/tiny-social-net/security"

	"gitlab.com/gzavodov/tiny-social-net/api/users"
	"gitlab.com/gzavodov/tiny-social-net/domain"

	"gitlab.com/gzavodov/tiny-social-net/web/middleware"

	"gitlab.com/gzavodov/tiny-social-net/internal/cookies"

	"gitlab.com/gzavodov/tiny-social-net/web/actions"
	"gitlab.com/gzavodov/tiny-social-net/web/pages"

	"github.com/urfave/negroni"

	"github.com/gin-gonic/gin"
	"gitlab.com/gzavodov/tiny-social-net/internal/database/mysql"

	"gitlab.com/gzavodov/tiny-social-net/config"
)

func main() {
	configFilePath := flag.String("config", "", "Path to configuration file")
	flag.Parse()

	conf := &config.Configuration{}
	err := conf.Load(
		*configFilePath,
		&config.Configuration{
			HTTPAddress: "127.0.0.1:8080",
		},
	)
	if err != nil {
		log.Fatalf("could not load configuration: %v", err)
	}

	masterDbConn := mustConnectToDb(
		conf.DSN,
		time.Duration(conf.DbConnMaxLifetime)*time.Minute,
		int(conf.MaxOpenDbConns),
		int(conf.MaxIdleDbConns),
	)

	defer masterDbConn.Close()

	replicaDbConns := make([]*sqlx.DB, 0, 2)
	for _, dataSourceName := range conf.ReplicaDSN {
		replicaDbConns = append(
			replicaDbConns,
			mustConnectToDb(
				dataSourceName,
				time.Duration(conf.DbConnMaxLifetime)*time.Minute,
				int(conf.MaxOpenDbConns),
				int(conf.MaxIdleDbConns),
			),
		)
	}

	defer func() {
		for _, dbConn := range replicaDbConns {
			dbConn.Close()
		}
	}()

	loadBalancer := balancers.NewLoadBalancer(masterDbConn, replicaDbConns)

	userRepo := mysql.NewUserRepository(loadBalancer)
	profileViewRepo := mysql.NewProfileViewRepository(masterDbConn)
	postRepo := mysql.NewPostRepository(masterDbConn)
	followerRepo := mysql.NewFollowerRepository(masterDbConn)
	likeRepo := mysql.NewLikeRepository(masterDbConn)

	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(time.Hour))
	if err != nil {
		log.Fatalf("could not create cache: %v", err)
	}

	feedCache := caches.NewNewsFeed(
		cache,
		caches.WithBatchInvalidation(conf.CacheInvalidationBatchSize, conf.CacheInvalidationDelay),
	)

	router := gin.Default()

	prepareApiRoutes(router, userRepo, postRepo, followerRepo, likeRepo, feedCache, conf)
	prepareWebRoutes(router, userRepo, profileViewRepo, postRepo, followerRepo, likeRepo, feedCache)

	server := negroni.Classic()
	server.UseHandler(router)

	server.Run(conf.HTTPAddress)
}

func mustConnectToDb(dataSourceName string, maxLifetime time.Duration, maxOpenConns int, maxIdleConns int) *sqlx.DB {
	dbConn := sqlx.MustConnect("mysql", dataSourceName)
	if err := dbConn.Ping(); err != nil {
		log.Fatal(err)
	}

	dbConn.SetConnMaxLifetime(maxLifetime)
	dbConn.SetMaxOpenConns(maxOpenConns)
	dbConn.SetMaxIdleConns(maxIdleConns)

	return dbConn
}

func prepareApiRoutes(router *gin.Engine,
	userRepo domain.UserRepository,
	postRepo domain.PostRepository,
	followerRepo domain.FollowerRepository,
	likeRepo domain.LikeRepository,
	feedCache *caches.NewsFeedCache,
	conf *config.Configuration,
) {
	api := router.Group("/api/v2")

	jwt := security.NewJWT(conf.SessionSecret)
	authHandler := api_middlewares.NewAuthenticationHandler(jwt)

	user := api.Group("/user")

	userApiHandler := users.NewHandler(jwt, userRepo)
	user.POST("/signup", authHandler.EnsureNotLoggedIn, userApiHandler.Signup)
	user.POST("/login", authHandler.EnsureNotLoggedIn, userApiHandler.Login)
	user.GET("/find", authHandler.EnsureLoggedIn, userApiHandler.Find)

	profile := api.Group("/profile")

	profileApiHandler := profiles.NewHandler(userRepo, jwt)
	profile.GET("/view/:id", authHandler.EnsureLoggedIn, profileApiHandler.Get)
	profile.GET("/view", authHandler.EnsureLoggedIn, profileApiHandler.Get)
	profile.POST("/update", authHandler.EnsureLoggedIn, profileApiHandler.Update)

	follower := api.Group("/follower")

	followerApiHandler := followers.NewHandler(followerRepo, userRepo, jwt, feedCache)
	follower.POST("/follow", authHandler.EnsureLoggedIn, followerApiHandler.Follow)
	follower.POST("/unfollow", authHandler.EnsureLoggedIn, followerApiHandler.Unfollow)
	follower.GET("/follow-by/list/:id", authHandler.EnsureLoggedIn, followerApiHandler.GetFollowByList)
	follower.GET("/follow-by/list", authHandler.EnsureLoggedIn, followerApiHandler.GetFollowByList)
	follower.GET("/follow-to/list/:id", authHandler.EnsureLoggedIn, followerApiHandler.GetFollowToList)
	follower.GET("/follow-to/list", authHandler.EnsureLoggedIn, followerApiHandler.GetFollowToList)

	post := api.Group("/post")

	postApiHandler := posts.NewHandler(postRepo, followerRepo, jwt, feedCache)
	post.POST("/create", authHandler.EnsureLoggedIn, postApiHandler.Create)
	post.GET("/get/:id", authHandler.EnsureLoggedIn, postApiHandler.Get)
	post.POST("/update", authHandler.EnsureLoggedIn, postApiHandler.Update)
	post.POST("/delete", authHandler.EnsureLoggedIn, postApiHandler.Delete)

	like := api.Group("/like")

	likeApiHandler := likes.NewHandler(likeRepo, jwt)
	like.POST("/add", authHandler.EnsureLoggedIn, likeApiHandler.Add)
	like.POST("/remove", authHandler.EnsureLoggedIn, likeApiHandler.Remove)
	like.GET("/get-count/:id", authHandler.EnsureLoggedIn, likeApiHandler.GetCount)

	feed := api.Group("/feed")

	feedApiHandler := feeds.NewHandler(userRepo, jwt, feedCache, postRepo)
	feed.GET("/news", authHandler.EnsureLoggedIn, feedApiHandler.News)
}

func prepareWebRoutes(
	router *gin.Engine,
	userRepo domain.UserRepository,
	profileViewRepo domain.ProfileViewRepository,
	postRepo domain.PostRepository,
	followerRepo domain.FollowerRepository,
	likeRepo domain.LikeRepository,
	feedCache *caches.NewsFeedCache,
) {
	cookiesStorage := cookies.NewUserSessionStorage()
	authController := middleware.NewAuthenticationController(cookiesStorage)

	router.LoadHTMLGlob("views/*.html")

	user := router.Group("/user")

	userActionController := actions.NewUserController(userRepo, cookiesStorage)
	user.POST("/signup", authController.EnsureNotLoggedIn, userActionController.UserSignup)
	user.POST("/login", authController.EnsureNotLoggedIn, userActionController.UserLogin)

	indexController := pages.NewIndexController(postRepo, userRepo)
	router.GET("/", authController.EnsureLoggedIn, indexController.Default)

	welcomeController := pages.NewWelcomeController()
	router.GET("/welcome", authController.EnsureNotLoggedIn, welcomeController.Default)

	userPageController := pages.NewUserController(userRepo, cookiesStorage)
	router.GET("/signup", authController.EnsureNotLoggedIn, userPageController.Signup)
	router.GET("/login", authController.EnsureNotLoggedIn, userPageController.Login)
	router.GET("/logout", authController.EnsureLoggedIn, userPageController.Logout)
	router.GET("/explore", authController.EnsureLoggedIn, userPageController.Explore)

	postPageController := pages.NewPostController(postRepo, likeRepo)
	router.GET("/create_post", authController.EnsureLoggedIn, postPageController.CreatePost)
	router.GET("/view_post/:id", authController.EnsureLoggedIn, postPageController.ViewPost)
	router.GET("/edit_post/:id", authController.EnsureLoggedIn, postPageController.EditPost)

	postApiController := actions.NewPostController(postRepo, followerRepo, feedCache)
	api := router.Group("/api")
	api.POST("/create_new_post", authController.EnsureLoggedIn, postApiController.CreatePost)
	api.POST("/update_post", authController.EnsureLoggedIn, postApiController.UpdatePost)
	api.POST("/delete_post", authController.EnsureLoggedIn, postApiController.DeletePost)

	followerApiController := actions.NewFollowerController(followerRepo, userRepo)
	api.POST("/follow", authController.EnsureLoggedIn, followerApiController.Follow)
	api.POST("/unfollow", authController.EnsureLoggedIn, followerApiController.Unfollow)

	followerPageController := pages.NewFollowerController(followerRepo, userRepo)
	router.GET("/followers/:id", authController.EnsureLoggedIn, followerPageController.Followers)
	router.GET("/followings/:id", authController.EnsureLoggedIn, followerPageController.Followings)

	profileController := actions.NewProfileController(userRepo)
	api.POST("/update_profile", authController.EnsureLoggedIn, profileController.Update)
	api.POST("/change_avatar", authController.EnsureLoggedIn, profileController.ChangeAvatar)

	profilePageController := pages.NewProfileController(userRepo, profileViewRepo, followerRepo, postRepo)
	router.GET("/profile/:id", authController.EnsureLoggedIn, profilePageController.View)
	router.GET("/edit_profile", authController.EnsureLoggedIn, profilePageController.Edit)

	likeController := actions.NewLikeController(likeRepo)
	api.POST("/like", authController.EnsureLoggedIn, likeController.Like)
	api.POST("/unlike", authController.EnsureLoggedIn, likeController.Unlike)

	//apiGroup.POST("/deactivate-account", R.DeactivateAcc)

	notFoundController := pages.NewNotFoundController()
	router.GET("/404", notFoundController.Default)

	router.GET("/profile", notFoundController.Default)

	router.GET("/view_post", notFoundController.Default)
	router.GET("/edit_post", notFoundController.Default)

	router.GET("/followers", notFoundController.Default)
	router.GET("/followings", notFoundController.Default)
}
