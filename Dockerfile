# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Builder
FROM golang:1.18 as builder

RUN mkdir -p /opt/tiny-social-net
RUN mkdir -p /opt/script
WORKDIR /opt/tiny-social-net

COPY ./script/wait-for-it.sh /opt/script/wait-for-it.sh

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .
RUN CGO_ENABLED=0 go build -o /opt/tiny-social-net/app ./cmd/tiny-social-net/.

# Release
FROM alpine:latest
LABEL maintainer="Grigoriy Zavodov <zavodov@gmail.com>"

RUN mkdir -p /tiny-social-net
WORKDIR /tiny-social-net

COPY --from=builder /opt/tiny-social-net ./
COPY --from=builder /opt/script/wait-for-it.sh /usr/bin/wait-for-it.sh
RUN chmod +x /usr/bin/wait-for-it.sh
RUN apk update && apk add bash && apk add --no-cache coreutils

EXPOSE 9090
ENTRYPOINT [ "./app" ]
